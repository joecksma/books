import 'package:books/models/book/book_model.dart';
import 'package:books/models/failures/firebase_failure.dart';
import 'package:books/providers/auth_provider.dart';
import 'package:books/repositories/book_repository.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

final bookListFailureProvider = StateProvider<FirebaseFailure?>((_) => null);

final bookListControllerProvider =
    StateNotifierProvider<BookListController, AsyncValue<List<Book>>>((ref) {
  final user = ref.watch(authStateProvider);
  return user.when<BookListController>(
    data: (value) => BookListController(ref.read, value?.uid),
    loading: () => BookListController(ref.read, null),
    error: (error, stack) => BookListController(ref.read, null),
  );
});

class BookListController extends StateNotifier<AsyncValue<List<Book>>> {
  final Reader _read;
  late final String? _userId;

  BookListController(this._read, this._userId)
      : super(const AsyncValue.loading()) {
    if (_userId != null) {
      readBooks();
    }
  }

  Future<void> readBooks({bool isRefreshing = false}) async {
    if (isRefreshing) state = const AsyncValue.loading();

    final booksOrFailure =
        await _read(bookRepositoryProvider).readBooks(userId: _userId!);
    booksOrFailure.fold(
      (failure) => state = AsyncValue.error(failure),
      (books) => {
        if (mounted)
          {
            state = AsyncValue.data(books),
          }
      },
    );
  }

  Future<void> addBook({required Book book}) async {
    //TODO Improvement: createBook maybe check if such a book already exists?
    // also need to display a message to the user if this happens
    final time = DateTime.now();
    final bookIdOrFailure = await _read(bookRepositoryProvider).createBook(
      userId: _userId!,
      book: book.copyWith(time: time, updatedAt: time),
    );
    bookIdOrFailure.fold(
      (failure) => _read(bookListFailureProvider).state = failure,
      (bookId) => state.whenData((books) =>
          state = AsyncValue.data(books..insert(0, book.copyWith(id: bookId)))),
    );
  }

  Future<void> updateBook({required Book updatedBook}) async {
    final voidOrFailure = await _read(bookRepositoryProvider).updateBook(
      userId: _userId!,
      book: updatedBook.copyWith(updatedAt: DateTime.now()),
    );
    voidOrFailure.fold(
      (failure) => _read(bookListFailureProvider).state = failure,
      (value) => state.whenData((books) => {
            state = AsyncValue.data([
              for (final book in books)
                if (book.id == updatedBook.id) updatedBook else book
            ])
          }),
    );
  }

  Future<void> deleteBook({required String bookId}) async {
    final voidOrFailure = await _read(bookRepositoryProvider).deleteBook(
      userId: _userId!,
      bookId: bookId,
    );
    voidOrFailure.fold(
      (failure) => _read(bookListFailureProvider).state = failure,
      (value) => state.whenData(
        (books) => state =
            AsyncValue.data(books..removeWhere((book) => book.id == bookId)),
      ),
    );
  }
}
