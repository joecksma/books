import 'package:books/models/filter/filter_model.dart';
import 'package:books/providers/filter_provider.dart';
import 'package:books/repositories/filter_repository.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

final filterControllerProvider =
    StateNotifierProvider<FilterController, Filter>((ref) {
  return FilterController(ref.read);
});

class FilterController extends StateNotifier<Filter> {
  final Reader _read;

  FilterController(this._read) : super(Filter.empty()) {
    readFilter();
  }

  Future<void> readFilter() async {
    state = await getSavedFilter();
  }

  Future<Filter> getSavedFilter() async {
    return _read(filterRepositoryProvider).readFilter();
  }

  Future<bool> saveFilter({required Filter filter}) async {
    return _read(filterRepositoryProvider)
        .setFilter(filter: _fixLength(filter));
  }

  Future<void> setFilter({required Filter filter}) async {
    state = filter;
  }

  Future<void> deleteFilter() async {
    return _read(filterRepositoryProvider).deleteFilter();
  }

  Future<void> resetToSavedSort() async {
    final filter = await getSavedFilter();
    state = state.copyWith(sort: filter.sort);
  }

  Future<void> resetSort() async {
    state = _fixLength(state.copyWith(sort: Filter.empty().sort));
  }

  Future<void> resetToSavedFilter() async {
    final filter = await getSavedFilter();
    state = filter.copyWith(sort: state.sort);
  }

  Future<void> clearFilter() async {
    state = _fixLength(Filter.empty().copyWith(sort: state.sort));
  }

  Filter _fixLength(Filter filter) {
    final available = _read(availableFilterProvider).state;
    double filterMinLength = filter.minLength == double.infinity ||
            filter.minLength < available.minLength
        ? available.minLength
        : filter.minLength;
    double filterMaxLength = filter.maxLength == double.negativeInfinity ||
            filter.maxLength > available.maxLength
        ? available.maxLength
        : filter.maxLength;
    if (filterMinLength > filterMaxLength) {
      filterMinLength = available.minLength;
      filterMaxLength = available.maxLength;
    }
    return filter.copyWith(
      minLength: filterMinLength,
      maxLength: filterMaxLength,
    );
  }
}
