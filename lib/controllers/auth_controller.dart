import 'dart:async';

import 'package:books/models/failures/auth_failure.dart';
import 'package:books/repositories/auth_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class AuthController extends StateNotifier<User?> {
  final Reader _read;

  StreamSubscription<User?>? _authStateChangesSubscription;

  AuthController(this._read) : super(null) {
    _authStateChangesSubscription?.cancel();
    _authStateChangesSubscription = _read(authRepositoryProvider)
        .authStateChanges
        .listen((user) => state = user);
  }

  Stream<User?> get authStateChange =>
      _read(authRepositoryProvider).authStateChanges;

  @override
  void dispose() {
    _authStateChangesSubscription?.cancel();
    super.dispose();
  }

  Future<Either<AuthFailure, String>> signIn(
      {required String email, required String password}) async {
    if (email.isNotEmpty && password.isNotEmpty) {
      return _read(authRepositoryProvider).signIn(
        email: email,
        password: password,
      );
    } else {
      return left(AuthFailure(message: "Please input your Email and Password"));
    }
  }

  Future<Either<AuthFailure, String>> signUp(
      {required String email, required String password}) async {
    if (email.isNotEmpty && password.isNotEmpty) {
      return _read(authRepositoryProvider).signUp(
        email: email,
        password: password,
      );
    } else {
      return left(
          AuthFailure(message: "Please choose your Email and Password"));
    }
  }

  Future<Either<AuthFailure, String>> signout() async {
    return _read(authRepositoryProvider).signOut();
  }
}
