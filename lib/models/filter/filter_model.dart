import 'package:books/models/filter/filter_list_model.dart';
import 'package:books/shared/enums.dart';
import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'filter_model.freezed.dart';
part 'filter_model.g.dart';

@freezed
abstract class Filter implements _$Filter {
  const Filter._();

  @JsonSerializable(explicitToJson: true)
  const factory Filter({
    required Set<String> status,
    required FilterList sort, // include.first == sort, and == direction
    required double minLength,
    required double maxLength,
    required FilterList ratings,
    required FilterList tags,
    required FilterList characters,
    required FilterList genres,
  }) = _Filter;

  factory Filter.empty() => const Filter(
        status: {statusUnRead},
        sort: FilterList(include: {sortCreationTime}, and: true),
        minLength: double.negativeInfinity,
        maxLength: double.infinity,
        ratings: FilterList(and: false),
        tags: FilterList(),
        characters: FilterList(),
        genres: FilterList(),
      );

  factory Filter.fromJson(Map<String, dynamic> json) => _$FilterFromJson(json);
}
