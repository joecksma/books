import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'filter_list_model.freezed.dart';
part 'filter_list_model.g.dart';

@freezed
abstract class FilterList implements _$FilterList {
  const FilterList._();

  @JsonSerializable(explicitToJson: true)
  const factory FilterList({
    @Default({}) Set<String> include,
    @Default({}) Set<String> exclude,
    @Default(true) bool and,
  }) = _FilterList;

  bool get active => include.isNotEmpty || exclude.isNotEmpty;

  factory FilterList.fromJson(Map<String, dynamic> json) =>
      _$FilterListFromJson(json);
}
