// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'filter_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Filter _$_$_FilterFromJson(Map<String, dynamic> json) {
  return _$_Filter(
    status: (json['status'] as List<dynamic>).map((e) => e as String).toSet(),
    sort: FilterList.fromJson(json['sort'] as Map<String, dynamic>),
    minLength: (json['minLength'] as num).toDouble(),
    maxLength: (json['maxLength'] as num).toDouble(),
    ratings: FilterList.fromJson(json['ratings'] as Map<String, dynamic>),
    tags: FilterList.fromJson(json['tags'] as Map<String, dynamic>),
    characters: FilterList.fromJson(json['characters'] as Map<String, dynamic>),
    genres: FilterList.fromJson(json['genres'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$_$_FilterToJson(_$_Filter instance) => <String, dynamic>{
      'status': instance.status.toList(),
      'sort': instance.sort.toJson(),
      'minLength': instance.minLength,
      'maxLength': instance.maxLength,
      'ratings': instance.ratings.toJson(),
      'tags': instance.tags.toJson(),
      'characters': instance.characters.toJson(),
      'genres': instance.genres.toJson(),
    };
