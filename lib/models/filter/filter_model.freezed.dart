// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'filter_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Filter _$FilterFromJson(Map<String, dynamic> json) {
  return _Filter.fromJson(json);
}

/// @nodoc
class _$FilterTearOff {
  const _$FilterTearOff();

  _Filter call(
      {required Set<String> status,
      required FilterList sort,
      required double minLength,
      required double maxLength,
      required FilterList ratings,
      required FilterList tags,
      required FilterList characters,
      required FilterList genres}) {
    return _Filter(
      status: status,
      sort: sort,
      minLength: minLength,
      maxLength: maxLength,
      ratings: ratings,
      tags: tags,
      characters: characters,
      genres: genres,
    );
  }

  Filter fromJson(Map<String, Object> json) {
    return Filter.fromJson(json);
  }
}

/// @nodoc
const $Filter = _$FilterTearOff();

/// @nodoc
mixin _$Filter {
  Set<String> get status => throw _privateConstructorUsedError;
  FilterList get sort =>
      throw _privateConstructorUsedError; // include.first == sort, and == direction
  double get minLength => throw _privateConstructorUsedError;
  double get maxLength => throw _privateConstructorUsedError;
  FilterList get ratings => throw _privateConstructorUsedError;
  FilterList get tags => throw _privateConstructorUsedError;
  FilterList get characters => throw _privateConstructorUsedError;
  FilterList get genres => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $FilterCopyWith<Filter> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FilterCopyWith<$Res> {
  factory $FilterCopyWith(Filter value, $Res Function(Filter) then) =
      _$FilterCopyWithImpl<$Res>;
  $Res call(
      {Set<String> status,
      FilterList sort,
      double minLength,
      double maxLength,
      FilterList ratings,
      FilterList tags,
      FilterList characters,
      FilterList genres});

  $FilterListCopyWith<$Res> get sort;
  $FilterListCopyWith<$Res> get ratings;
  $FilterListCopyWith<$Res> get tags;
  $FilterListCopyWith<$Res> get characters;
  $FilterListCopyWith<$Res> get genres;
}

/// @nodoc
class _$FilterCopyWithImpl<$Res> implements $FilterCopyWith<$Res> {
  _$FilterCopyWithImpl(this._value, this._then);

  final Filter _value;
  // ignore: unused_field
  final $Res Function(Filter) _then;

  @override
  $Res call({
    Object? status = freezed,
    Object? sort = freezed,
    Object? minLength = freezed,
    Object? maxLength = freezed,
    Object? ratings = freezed,
    Object? tags = freezed,
    Object? characters = freezed,
    Object? genres = freezed,
  }) {
    return _then(_value.copyWith(
      status: status == freezed
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as Set<String>,
      sort: sort == freezed
          ? _value.sort
          : sort // ignore: cast_nullable_to_non_nullable
              as FilterList,
      minLength: minLength == freezed
          ? _value.minLength
          : minLength // ignore: cast_nullable_to_non_nullable
              as double,
      maxLength: maxLength == freezed
          ? _value.maxLength
          : maxLength // ignore: cast_nullable_to_non_nullable
              as double,
      ratings: ratings == freezed
          ? _value.ratings
          : ratings // ignore: cast_nullable_to_non_nullable
              as FilterList,
      tags: tags == freezed
          ? _value.tags
          : tags // ignore: cast_nullable_to_non_nullable
              as FilterList,
      characters: characters == freezed
          ? _value.characters
          : characters // ignore: cast_nullable_to_non_nullable
              as FilterList,
      genres: genres == freezed
          ? _value.genres
          : genres // ignore: cast_nullable_to_non_nullable
              as FilterList,
    ));
  }

  @override
  $FilterListCopyWith<$Res> get sort {
    return $FilterListCopyWith<$Res>(_value.sort, (value) {
      return _then(_value.copyWith(sort: value));
    });
  }

  @override
  $FilterListCopyWith<$Res> get ratings {
    return $FilterListCopyWith<$Res>(_value.ratings, (value) {
      return _then(_value.copyWith(ratings: value));
    });
  }

  @override
  $FilterListCopyWith<$Res> get tags {
    return $FilterListCopyWith<$Res>(_value.tags, (value) {
      return _then(_value.copyWith(tags: value));
    });
  }

  @override
  $FilterListCopyWith<$Res> get characters {
    return $FilterListCopyWith<$Res>(_value.characters, (value) {
      return _then(_value.copyWith(characters: value));
    });
  }

  @override
  $FilterListCopyWith<$Res> get genres {
    return $FilterListCopyWith<$Res>(_value.genres, (value) {
      return _then(_value.copyWith(genres: value));
    });
  }
}

/// @nodoc
abstract class _$FilterCopyWith<$Res> implements $FilterCopyWith<$Res> {
  factory _$FilterCopyWith(_Filter value, $Res Function(_Filter) then) =
      __$FilterCopyWithImpl<$Res>;
  @override
  $Res call(
      {Set<String> status,
      FilterList sort,
      double minLength,
      double maxLength,
      FilterList ratings,
      FilterList tags,
      FilterList characters,
      FilterList genres});

  @override
  $FilterListCopyWith<$Res> get sort;
  @override
  $FilterListCopyWith<$Res> get ratings;
  @override
  $FilterListCopyWith<$Res> get tags;
  @override
  $FilterListCopyWith<$Res> get characters;
  @override
  $FilterListCopyWith<$Res> get genres;
}

/// @nodoc
class __$FilterCopyWithImpl<$Res> extends _$FilterCopyWithImpl<$Res>
    implements _$FilterCopyWith<$Res> {
  __$FilterCopyWithImpl(_Filter _value, $Res Function(_Filter) _then)
      : super(_value, (v) => _then(v as _Filter));

  @override
  _Filter get _value => super._value as _Filter;

  @override
  $Res call({
    Object? status = freezed,
    Object? sort = freezed,
    Object? minLength = freezed,
    Object? maxLength = freezed,
    Object? ratings = freezed,
    Object? tags = freezed,
    Object? characters = freezed,
    Object? genres = freezed,
  }) {
    return _then(_Filter(
      status: status == freezed
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as Set<String>,
      sort: sort == freezed
          ? _value.sort
          : sort // ignore: cast_nullable_to_non_nullable
              as FilterList,
      minLength: minLength == freezed
          ? _value.minLength
          : minLength // ignore: cast_nullable_to_non_nullable
              as double,
      maxLength: maxLength == freezed
          ? _value.maxLength
          : maxLength // ignore: cast_nullable_to_non_nullable
              as double,
      ratings: ratings == freezed
          ? _value.ratings
          : ratings // ignore: cast_nullable_to_non_nullable
              as FilterList,
      tags: tags == freezed
          ? _value.tags
          : tags // ignore: cast_nullable_to_non_nullable
              as FilterList,
      characters: characters == freezed
          ? _value.characters
          : characters // ignore: cast_nullable_to_non_nullable
              as FilterList,
      genres: genres == freezed
          ? _value.genres
          : genres // ignore: cast_nullable_to_non_nullable
              as FilterList,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_Filter extends _Filter with DiagnosticableTreeMixin {
  const _$_Filter(
      {required this.status,
      required this.sort,
      required this.minLength,
      required this.maxLength,
      required this.ratings,
      required this.tags,
      required this.characters,
      required this.genres})
      : super._();

  factory _$_Filter.fromJson(Map<String, dynamic> json) =>
      _$_$_FilterFromJson(json);

  @override
  final Set<String> status;
  @override
  final FilterList sort;
  @override // include.first == sort, and == direction
  final double minLength;
  @override
  final double maxLength;
  @override
  final FilterList ratings;
  @override
  final FilterList tags;
  @override
  final FilterList characters;
  @override
  final FilterList genres;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Filter(status: $status, sort: $sort, minLength: $minLength, maxLength: $maxLength, ratings: $ratings, tags: $tags, characters: $characters, genres: $genres)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'Filter'))
      ..add(DiagnosticsProperty('status', status))
      ..add(DiagnosticsProperty('sort', sort))
      ..add(DiagnosticsProperty('minLength', minLength))
      ..add(DiagnosticsProperty('maxLength', maxLength))
      ..add(DiagnosticsProperty('ratings', ratings))
      ..add(DiagnosticsProperty('tags', tags))
      ..add(DiagnosticsProperty('characters', characters))
      ..add(DiagnosticsProperty('genres', genres));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Filter &&
            (identical(other.status, status) ||
                const DeepCollectionEquality().equals(other.status, status)) &&
            (identical(other.sort, sort) ||
                const DeepCollectionEquality().equals(other.sort, sort)) &&
            (identical(other.minLength, minLength) ||
                const DeepCollectionEquality()
                    .equals(other.minLength, minLength)) &&
            (identical(other.maxLength, maxLength) ||
                const DeepCollectionEquality()
                    .equals(other.maxLength, maxLength)) &&
            (identical(other.ratings, ratings) ||
                const DeepCollectionEquality()
                    .equals(other.ratings, ratings)) &&
            (identical(other.tags, tags) ||
                const DeepCollectionEquality().equals(other.tags, tags)) &&
            (identical(other.characters, characters) ||
                const DeepCollectionEquality()
                    .equals(other.characters, characters)) &&
            (identical(other.genres, genres) ||
                const DeepCollectionEquality().equals(other.genres, genres)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(status) ^
      const DeepCollectionEquality().hash(sort) ^
      const DeepCollectionEquality().hash(minLength) ^
      const DeepCollectionEquality().hash(maxLength) ^
      const DeepCollectionEquality().hash(ratings) ^
      const DeepCollectionEquality().hash(tags) ^
      const DeepCollectionEquality().hash(characters) ^
      const DeepCollectionEquality().hash(genres);

  @JsonKey(ignore: true)
  @override
  _$FilterCopyWith<_Filter> get copyWith =>
      __$FilterCopyWithImpl<_Filter>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_FilterToJson(this);
  }
}

abstract class _Filter extends Filter {
  const factory _Filter(
      {required Set<String> status,
      required FilterList sort,
      required double minLength,
      required double maxLength,
      required FilterList ratings,
      required FilterList tags,
      required FilterList characters,
      required FilterList genres}) = _$_Filter;
  const _Filter._() : super._();

  factory _Filter.fromJson(Map<String, dynamic> json) = _$_Filter.fromJson;

  @override
  Set<String> get status => throw _privateConstructorUsedError;
  @override
  FilterList get sort => throw _privateConstructorUsedError;
  @override // include.first == sort, and == direction
  double get minLength => throw _privateConstructorUsedError;
  @override
  double get maxLength => throw _privateConstructorUsedError;
  @override
  FilterList get ratings => throw _privateConstructorUsedError;
  @override
  FilterList get tags => throw _privateConstructorUsedError;
  @override
  FilterList get characters => throw _privateConstructorUsedError;
  @override
  FilterList get genres => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$FilterCopyWith<_Filter> get copyWith => throw _privateConstructorUsedError;
}
