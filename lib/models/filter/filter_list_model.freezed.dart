// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'filter_list_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

FilterList _$FilterListFromJson(Map<String, dynamic> json) {
  return _FilterList.fromJson(json);
}

/// @nodoc
class _$FilterListTearOff {
  const _$FilterListTearOff();

  _FilterList call(
      {Set<String> include = const {},
      Set<String> exclude = const {},
      bool and = true}) {
    return _FilterList(
      include: include,
      exclude: exclude,
      and: and,
    );
  }

  FilterList fromJson(Map<String, Object> json) {
    return FilterList.fromJson(json);
  }
}

/// @nodoc
const $FilterList = _$FilterListTearOff();

/// @nodoc
mixin _$FilterList {
  Set<String> get include => throw _privateConstructorUsedError;
  Set<String> get exclude => throw _privateConstructorUsedError;
  bool get and => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $FilterListCopyWith<FilterList> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FilterListCopyWith<$Res> {
  factory $FilterListCopyWith(
          FilterList value, $Res Function(FilterList) then) =
      _$FilterListCopyWithImpl<$Res>;
  $Res call({Set<String> include, Set<String> exclude, bool and});
}

/// @nodoc
class _$FilterListCopyWithImpl<$Res> implements $FilterListCopyWith<$Res> {
  _$FilterListCopyWithImpl(this._value, this._then);

  final FilterList _value;
  // ignore: unused_field
  final $Res Function(FilterList) _then;

  @override
  $Res call({
    Object? include = freezed,
    Object? exclude = freezed,
    Object? and = freezed,
  }) {
    return _then(_value.copyWith(
      include: include == freezed
          ? _value.include
          : include // ignore: cast_nullable_to_non_nullable
              as Set<String>,
      exclude: exclude == freezed
          ? _value.exclude
          : exclude // ignore: cast_nullable_to_non_nullable
              as Set<String>,
      and: and == freezed
          ? _value.and
          : and // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
abstract class _$FilterListCopyWith<$Res> implements $FilterListCopyWith<$Res> {
  factory _$FilterListCopyWith(
          _FilterList value, $Res Function(_FilterList) then) =
      __$FilterListCopyWithImpl<$Res>;
  @override
  $Res call({Set<String> include, Set<String> exclude, bool and});
}

/// @nodoc
class __$FilterListCopyWithImpl<$Res> extends _$FilterListCopyWithImpl<$Res>
    implements _$FilterListCopyWith<$Res> {
  __$FilterListCopyWithImpl(
      _FilterList _value, $Res Function(_FilterList) _then)
      : super(_value, (v) => _then(v as _FilterList));

  @override
  _FilterList get _value => super._value as _FilterList;

  @override
  $Res call({
    Object? include = freezed,
    Object? exclude = freezed,
    Object? and = freezed,
  }) {
    return _then(_FilterList(
      include: include == freezed
          ? _value.include
          : include // ignore: cast_nullable_to_non_nullable
              as Set<String>,
      exclude: exclude == freezed
          ? _value.exclude
          : exclude // ignore: cast_nullable_to_non_nullable
              as Set<String>,
      and: and == freezed
          ? _value.and
          : and // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_FilterList extends _FilterList with DiagnosticableTreeMixin {
  const _$_FilterList(
      {this.include = const {}, this.exclude = const {}, this.and = true})
      : super._();

  factory _$_FilterList.fromJson(Map<String, dynamic> json) =>
      _$_$_FilterListFromJson(json);

  @JsonKey(defaultValue: const {})
  @override
  final Set<String> include;
  @JsonKey(defaultValue: const {})
  @override
  final Set<String> exclude;
  @JsonKey(defaultValue: true)
  @override
  final bool and;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'FilterList(include: $include, exclude: $exclude, and: $and)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'FilterList'))
      ..add(DiagnosticsProperty('include', include))
      ..add(DiagnosticsProperty('exclude', exclude))
      ..add(DiagnosticsProperty('and', and));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _FilterList &&
            (identical(other.include, include) ||
                const DeepCollectionEquality()
                    .equals(other.include, include)) &&
            (identical(other.exclude, exclude) ||
                const DeepCollectionEquality()
                    .equals(other.exclude, exclude)) &&
            (identical(other.and, and) ||
                const DeepCollectionEquality().equals(other.and, and)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(include) ^
      const DeepCollectionEquality().hash(exclude) ^
      const DeepCollectionEquality().hash(and);

  @JsonKey(ignore: true)
  @override
  _$FilterListCopyWith<_FilterList> get copyWith =>
      __$FilterListCopyWithImpl<_FilterList>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_FilterListToJson(this);
  }
}

abstract class _FilterList extends FilterList {
  const factory _FilterList(
      {Set<String> include, Set<String> exclude, bool and}) = _$_FilterList;
  const _FilterList._() : super._();

  factory _FilterList.fromJson(Map<String, dynamic> json) =
      _$_FilterList.fromJson;

  @override
  Set<String> get include => throw _privateConstructorUsedError;
  @override
  Set<String> get exclude => throw _privateConstructorUsedError;
  @override
  bool get and => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$FilterListCopyWith<_FilterList> get copyWith =>
      throw _privateConstructorUsedError;
}
