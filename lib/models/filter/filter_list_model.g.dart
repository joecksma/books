// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'filter_list_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_FilterList _$_$_FilterListFromJson(Map<String, dynamic> json) {
  return _$_FilterList(
    include:
        (json['include'] as List<dynamic>?)?.map((e) => e as String).toSet() ??
            {},
    exclude:
        (json['exclude'] as List<dynamic>?)?.map((e) => e as String).toSet() ??
            {},
    and: json['and'] as bool? ?? true,
  );
}

Map<String, dynamic> _$_$_FilterListToJson(_$_FilterList instance) =>
    <String, dynamic>{
      'include': instance.include.toList(),
      'exclude': instance.exclude.toList(),
      'and': instance.and,
    };
