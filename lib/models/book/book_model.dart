import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'book_model.freezed.dart';
part 'book_model.g.dart';

DateTime? _dateTimeAsIs(DateTime? dateTime) {
  return dateTime;
}

DateTime? _dateTimeFromTimestamp(Timestamp? timestamp) {
  return timestamp == null
      ? null
      : DateTime.parse(timestamp.toDate().toString());
}

@freezed
abstract class Book implements _$Book {
  const Book._();

  @JsonSerializable(explicitToJson: true)
  const factory Book(
      {String? id,
      required String title,
      required String author,
      required String description,
      required int length,
      required String rating,
      required String url,
      @JsonKey(name: 'time', fromJson: _dateTimeFromTimestamp, toJson: _dateTimeAsIs)
          DateTime? time,
      @JsonKey(name: 'updatedAt', fromJson: _dateTimeFromTimestamp, toJson: _dateTimeAsIs)
          DateTime? updatedAt,
      @Default(false)
          bool read,
      @Default([])
          List<String> genre,
      @Default([])
          List<String> characters,
      @Default([])
          List<String> lists}) = _Book;

  factory Book.empty() {
    final time = DateTime.now();
    return Book(
      title: '',
      author: '',
      url: '',
      time: time,
      updatedAt: time,
      description: '',
      length: 0,
      rating: '',
    );
  }

  factory Book.fromJson(Map<String, dynamic> json) => _$BookFromJson(json);

  factory Book.fromDocument(DocumentSnapshot doc) {
    final data = doc.data()!;
    return Book.fromJson(data).copyWith(id: doc.id);
  }

  Map<String, dynamic> toDocument() => toJson()..remove('id');
}
