// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'book_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Book _$BookFromJson(Map<String, dynamic> json) {
  return _Book.fromJson(json);
}

/// @nodoc
class _$BookTearOff {
  const _$BookTearOff();

  _Book call(
      {String? id,
      required String title,
      required String author,
      required String description,
      required int length,
      required String rating,
      required String url,
      @JsonKey(name: 'time', fromJson: _dateTimeFromTimestamp, toJson: _dateTimeAsIs)
          DateTime? time,
      @JsonKey(name: 'updatedAt', fromJson: _dateTimeFromTimestamp, toJson: _dateTimeAsIs)
          DateTime? updatedAt,
      bool read = false,
      List<String> genre = const [],
      List<String> characters = const [],
      List<String> lists = const []}) {
    return _Book(
      id: id,
      title: title,
      author: author,
      description: description,
      length: length,
      rating: rating,
      url: url,
      time: time,
      updatedAt: updatedAt,
      read: read,
      genre: genre,
      characters: characters,
      lists: lists,
    );
  }

  Book fromJson(Map<String, Object> json) {
    return Book.fromJson(json);
  }
}

/// @nodoc
const $Book = _$BookTearOff();

/// @nodoc
mixin _$Book {
  String? get id => throw _privateConstructorUsedError;
  String get title => throw _privateConstructorUsedError;
  String get author => throw _privateConstructorUsedError;
  String get description => throw _privateConstructorUsedError;
  int get length => throw _privateConstructorUsedError;
  String get rating => throw _privateConstructorUsedError;
  String get url => throw _privateConstructorUsedError;
  @JsonKey(
      name: 'time', fromJson: _dateTimeFromTimestamp, toJson: _dateTimeAsIs)
  DateTime? get time => throw _privateConstructorUsedError;
  @JsonKey(
      name: 'updatedAt',
      fromJson: _dateTimeFromTimestamp,
      toJson: _dateTimeAsIs)
  DateTime? get updatedAt => throw _privateConstructorUsedError;
  bool get read => throw _privateConstructorUsedError;
  List<String> get genre => throw _privateConstructorUsedError;
  List<String> get characters => throw _privateConstructorUsedError;
  List<String> get lists => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $BookCopyWith<Book> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BookCopyWith<$Res> {
  factory $BookCopyWith(Book value, $Res Function(Book) then) =
      _$BookCopyWithImpl<$Res>;
  $Res call(
      {String? id,
      String title,
      String author,
      String description,
      int length,
      String rating,
      String url,
      @JsonKey(name: 'time', fromJson: _dateTimeFromTimestamp, toJson: _dateTimeAsIs)
          DateTime? time,
      @JsonKey(name: 'updatedAt', fromJson: _dateTimeFromTimestamp, toJson: _dateTimeAsIs)
          DateTime? updatedAt,
      bool read,
      List<String> genre,
      List<String> characters,
      List<String> lists});
}

/// @nodoc
class _$BookCopyWithImpl<$Res> implements $BookCopyWith<$Res> {
  _$BookCopyWithImpl(this._value, this._then);

  final Book _value;
  // ignore: unused_field
  final $Res Function(Book) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? title = freezed,
    Object? author = freezed,
    Object? description = freezed,
    Object? length = freezed,
    Object? rating = freezed,
    Object? url = freezed,
    Object? time = freezed,
    Object? updatedAt = freezed,
    Object? read = freezed,
    Object? genre = freezed,
    Object? characters = freezed,
    Object? lists = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      author: author == freezed
          ? _value.author
          : author // ignore: cast_nullable_to_non_nullable
              as String,
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
      length: length == freezed
          ? _value.length
          : length // ignore: cast_nullable_to_non_nullable
              as int,
      rating: rating == freezed
          ? _value.rating
          : rating // ignore: cast_nullable_to_non_nullable
              as String,
      url: url == freezed
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String,
      time: time == freezed
          ? _value.time
          : time // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      updatedAt: updatedAt == freezed
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      read: read == freezed
          ? _value.read
          : read // ignore: cast_nullable_to_non_nullable
              as bool,
      genre: genre == freezed
          ? _value.genre
          : genre // ignore: cast_nullable_to_non_nullable
              as List<String>,
      characters: characters == freezed
          ? _value.characters
          : characters // ignore: cast_nullable_to_non_nullable
              as List<String>,
      lists: lists == freezed
          ? _value.lists
          : lists // ignore: cast_nullable_to_non_nullable
              as List<String>,
    ));
  }
}

/// @nodoc
abstract class _$BookCopyWith<$Res> implements $BookCopyWith<$Res> {
  factory _$BookCopyWith(_Book value, $Res Function(_Book) then) =
      __$BookCopyWithImpl<$Res>;
  @override
  $Res call(
      {String? id,
      String title,
      String author,
      String description,
      int length,
      String rating,
      String url,
      @JsonKey(name: 'time', fromJson: _dateTimeFromTimestamp, toJson: _dateTimeAsIs)
          DateTime? time,
      @JsonKey(name: 'updatedAt', fromJson: _dateTimeFromTimestamp, toJson: _dateTimeAsIs)
          DateTime? updatedAt,
      bool read,
      List<String> genre,
      List<String> characters,
      List<String> lists});
}

/// @nodoc
class __$BookCopyWithImpl<$Res> extends _$BookCopyWithImpl<$Res>
    implements _$BookCopyWith<$Res> {
  __$BookCopyWithImpl(_Book _value, $Res Function(_Book) _then)
      : super(_value, (v) => _then(v as _Book));

  @override
  _Book get _value => super._value as _Book;

  @override
  $Res call({
    Object? id = freezed,
    Object? title = freezed,
    Object? author = freezed,
    Object? description = freezed,
    Object? length = freezed,
    Object? rating = freezed,
    Object? url = freezed,
    Object? time = freezed,
    Object? updatedAt = freezed,
    Object? read = freezed,
    Object? genre = freezed,
    Object? characters = freezed,
    Object? lists = freezed,
  }) {
    return _then(_Book(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      author: author == freezed
          ? _value.author
          : author // ignore: cast_nullable_to_non_nullable
              as String,
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
      length: length == freezed
          ? _value.length
          : length // ignore: cast_nullable_to_non_nullable
              as int,
      rating: rating == freezed
          ? _value.rating
          : rating // ignore: cast_nullable_to_non_nullable
              as String,
      url: url == freezed
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String,
      time: time == freezed
          ? _value.time
          : time // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      updatedAt: updatedAt == freezed
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      read: read == freezed
          ? _value.read
          : read // ignore: cast_nullable_to_non_nullable
              as bool,
      genre: genre == freezed
          ? _value.genre
          : genre // ignore: cast_nullable_to_non_nullable
              as List<String>,
      characters: characters == freezed
          ? _value.characters
          : characters // ignore: cast_nullable_to_non_nullable
              as List<String>,
      lists: lists == freezed
          ? _value.lists
          : lists // ignore: cast_nullable_to_non_nullable
              as List<String>,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_Book extends _Book with DiagnosticableTreeMixin {
  const _$_Book(
      {this.id,
      required this.title,
      required this.author,
      required this.description,
      required this.length,
      required this.rating,
      required this.url,
      @JsonKey(name: 'time', fromJson: _dateTimeFromTimestamp, toJson: _dateTimeAsIs)
          this.time,
      @JsonKey(name: 'updatedAt', fromJson: _dateTimeFromTimestamp, toJson: _dateTimeAsIs)
          this.updatedAt,
      this.read = false,
      this.genre = const [],
      this.characters = const [],
      this.lists = const []})
      : super._();

  factory _$_Book.fromJson(Map<String, dynamic> json) =>
      _$_$_BookFromJson(json);

  @override
  final String? id;
  @override
  final String title;
  @override
  final String author;
  @override
  final String description;
  @override
  final int length;
  @override
  final String rating;
  @override
  final String url;
  @override
  @JsonKey(
      name: 'time', fromJson: _dateTimeFromTimestamp, toJson: _dateTimeAsIs)
  final DateTime? time;
  @override
  @JsonKey(
      name: 'updatedAt',
      fromJson: _dateTimeFromTimestamp,
      toJson: _dateTimeAsIs)
  final DateTime? updatedAt;
  @JsonKey(defaultValue: false)
  @override
  final bool read;
  @JsonKey(defaultValue: const [])
  @override
  final List<String> genre;
  @JsonKey(defaultValue: const [])
  @override
  final List<String> characters;
  @JsonKey(defaultValue: const [])
  @override
  final List<String> lists;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Book(id: $id, title: $title, author: $author, description: $description, length: $length, rating: $rating, url: $url, time: $time, updatedAt: $updatedAt, read: $read, genre: $genre, characters: $characters, lists: $lists)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'Book'))
      ..add(DiagnosticsProperty('id', id))
      ..add(DiagnosticsProperty('title', title))
      ..add(DiagnosticsProperty('author', author))
      ..add(DiagnosticsProperty('description', description))
      ..add(DiagnosticsProperty('length', length))
      ..add(DiagnosticsProperty('rating', rating))
      ..add(DiagnosticsProperty('url', url))
      ..add(DiagnosticsProperty('time', time))
      ..add(DiagnosticsProperty('updatedAt', updatedAt))
      ..add(DiagnosticsProperty('read', read))
      ..add(DiagnosticsProperty('genre', genre))
      ..add(DiagnosticsProperty('characters', characters))
      ..add(DiagnosticsProperty('lists', lists));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Book &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.title, title) ||
                const DeepCollectionEquality().equals(other.title, title)) &&
            (identical(other.author, author) ||
                const DeepCollectionEquality().equals(other.author, author)) &&
            (identical(other.description, description) ||
                const DeepCollectionEquality()
                    .equals(other.description, description)) &&
            (identical(other.length, length) ||
                const DeepCollectionEquality().equals(other.length, length)) &&
            (identical(other.rating, rating) ||
                const DeepCollectionEquality().equals(other.rating, rating)) &&
            (identical(other.url, url) ||
                const DeepCollectionEquality().equals(other.url, url)) &&
            (identical(other.time, time) ||
                const DeepCollectionEquality().equals(other.time, time)) &&
            (identical(other.updatedAt, updatedAt) ||
                const DeepCollectionEquality()
                    .equals(other.updatedAt, updatedAt)) &&
            (identical(other.read, read) ||
                const DeepCollectionEquality().equals(other.read, read)) &&
            (identical(other.genre, genre) ||
                const DeepCollectionEquality().equals(other.genre, genre)) &&
            (identical(other.characters, characters) ||
                const DeepCollectionEquality()
                    .equals(other.characters, characters)) &&
            (identical(other.lists, lists) ||
                const DeepCollectionEquality().equals(other.lists, lists)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(title) ^
      const DeepCollectionEquality().hash(author) ^
      const DeepCollectionEquality().hash(description) ^
      const DeepCollectionEquality().hash(length) ^
      const DeepCollectionEquality().hash(rating) ^
      const DeepCollectionEquality().hash(url) ^
      const DeepCollectionEquality().hash(time) ^
      const DeepCollectionEquality().hash(updatedAt) ^
      const DeepCollectionEquality().hash(read) ^
      const DeepCollectionEquality().hash(genre) ^
      const DeepCollectionEquality().hash(characters) ^
      const DeepCollectionEquality().hash(lists);

  @JsonKey(ignore: true)
  @override
  _$BookCopyWith<_Book> get copyWith =>
      __$BookCopyWithImpl<_Book>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_BookToJson(this);
  }
}

abstract class _Book extends Book {
  const factory _Book(
      {String? id,
      required String title,
      required String author,
      required String description,
      required int length,
      required String rating,
      required String url,
      @JsonKey(name: 'time', fromJson: _dateTimeFromTimestamp, toJson: _dateTimeAsIs)
          DateTime? time,
      @JsonKey(name: 'updatedAt', fromJson: _dateTimeFromTimestamp, toJson: _dateTimeAsIs)
          DateTime? updatedAt,
      bool read,
      List<String> genre,
      List<String> characters,
      List<String> lists}) = _$_Book;
  const _Book._() : super._();

  factory _Book.fromJson(Map<String, dynamic> json) = _$_Book.fromJson;

  @override
  String? get id => throw _privateConstructorUsedError;
  @override
  String get title => throw _privateConstructorUsedError;
  @override
  String get author => throw _privateConstructorUsedError;
  @override
  String get description => throw _privateConstructorUsedError;
  @override
  int get length => throw _privateConstructorUsedError;
  @override
  String get rating => throw _privateConstructorUsedError;
  @override
  String get url => throw _privateConstructorUsedError;
  @override
  @JsonKey(
      name: 'time', fromJson: _dateTimeFromTimestamp, toJson: _dateTimeAsIs)
  DateTime? get time => throw _privateConstructorUsedError;
  @override
  @JsonKey(
      name: 'updatedAt',
      fromJson: _dateTimeFromTimestamp,
      toJson: _dateTimeAsIs)
  DateTime? get updatedAt => throw _privateConstructorUsedError;
  @override
  bool get read => throw _privateConstructorUsedError;
  @override
  List<String> get genre => throw _privateConstructorUsedError;
  @override
  List<String> get characters => throw _privateConstructorUsedError;
  @override
  List<String> get lists => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$BookCopyWith<_Book> get copyWith => throw _privateConstructorUsedError;
}
