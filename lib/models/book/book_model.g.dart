// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'book_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Book _$_$_BookFromJson(Map<String, dynamic> json) {
  return _$_Book(
    id: json['id'] as String?,
    title: json['title'] as String,
    author: json['author'] as String,
    description: json['description'] as String,
    length: json['length'] as int,
    rating: json['rating'] as String,
    url: json['url'] as String,
    time: _dateTimeFromTimestamp(json['time'] as Timestamp?),
    updatedAt: _dateTimeFromTimestamp(json['updatedAt'] as Timestamp?),
    read: json['read'] as bool? ?? false,
    genre:
        (json['genre'] as List<dynamic>?)?.map((e) => e as String).toList() ??
            [],
    characters: (json['characters'] as List<dynamic>?)
            ?.map((e) => e as String)
            .toList() ??
        [],
    lists:
        (json['lists'] as List<dynamic>?)?.map((e) => e as String).toList() ??
            [],
  );
}

Map<String, dynamic> _$_$_BookToJson(_$_Book instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'author': instance.author,
      'description': instance.description,
      'length': instance.length,
      'rating': instance.rating,
      'url': instance.url,
      'time': _dateTimeAsIs(instance.time),
      'updatedAt': _dateTimeAsIs(instance.updatedAt),
      'read': instance.read,
      'genre': instance.genre,
      'characters': instance.characters,
      'lists': instance.lists,
    };
