import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:material_floating_search_bar/material_floating_search_bar.dart';

class FloatingSearchBarControllerHook
    extends Hook<FloatingSearchBarController> {
  const FloatingSearchBarControllerHook();
  @override
  _FloatingSearchBarControllerHookState createState() =>
      _FloatingSearchBarControllerHookState();
}

class _FloatingSearchBarControllerHookState extends HookState<
    FloatingSearchBarController, FloatingSearchBarControllerHook> {
  FloatingSearchBarController? controller;

  @override
  void initHook() {
    super.initHook();
    controller = FloatingSearchBarController();
  }

  @override
  FloatingSearchBarController build(BuildContext context) {
    return controller!;
  }

  @override
  void dispose() {
    super.dispose();
    controller?.dispose();
  }
}

FloatingSearchBarController useFloatingSearchBarController() {
  return use(const FloatingSearchBarControllerHook());
}
