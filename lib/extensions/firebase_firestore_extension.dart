import 'package:cloud_firestore/cloud_firestore.dart';

extension FirebaseFirestoreX on FirebaseFirestore {
  CollectionReference usersRef() => collection('users');
  DocumentReference userRef(String userId) => usersRef().doc(userId);
  CollectionReference booksRef(String userId) =>
      userRef(userId).collection('books');
}
