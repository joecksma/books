import 'package:another_flushbar/flushbar_helper.dart';
import 'package:books/controllers/book_controller.dart';
import 'package:books/models/failures/firebase_failure.dart';
import 'package:books/widgets/home_page/book_list/book_list_widget.dart';
import 'package:books/widgets/home_page/drawer/home_page_drawer_widget.dart';
import 'package:books/widgets/home_page/search_bar/search_bar_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class HomePage extends HookWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      drawer: HomePageDrawer(),
      body: SafeArea(
        child: Stack(
          fit: StackFit.expand,
          children: [
            Column(
              children: [
                const SizedBox(height: 60),
                ProviderListener(
                  onChange: (BuildContext context,
                      StateController<FirebaseFailure?> failure) {
                    FlushbarHelper.createError(message: failure.state!.message);
                  },
                  provider: bookListFailureProvider,
                  child: const Expanded(child: BookList()),
                ),
              ],
            ),
            SearchBarWidget(),
          ],
        ),
      ),
    );
  }
}
