import 'package:books/widgets/login_page/login_button_widget.dart';
import 'package:books/widgets/login_page/login_text_field_widget.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0),
          child: SingleChildScrollView(
            child: AutofillGroup(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const SizedBox(
                    height: 80,
                  ),
                  const Text(
                    'Sign-In',
                    style: TextStyle(
                      fontSize: 48,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(
                    height: 80,
                  ),
                  EmailField(),
                  const SizedBox(
                    height: 30,
                  ),
                  PasswordField(),
                  const SizedBox(
                    height: 30,
                  ),
                  LoginButton(),
                  // NOTE: disabled signup button if running in web
                  if (!kIsWeb) ...[
                    const Text('- OR -'),
                    SignUpButton(),
                  ],
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
