import 'package:books/providers/auth_provider.dart';
import 'package:books/providers/login_page_providers.dart';
import 'package:books/shared/constants.dart';
import 'package:books/widgets/login_page/sign_in_function.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class _LoginTextField extends StatelessWidget {
  final String text;
  final Function onPressed;
  final IconData icon;
  final TextInputType textInputType;
  final bool obscured;
  final List<String> autofillHints;
  final Function? onSubmitted;

  const _LoginTextField(
      {required this.text,
      required this.onPressed,
      required this.icon,
      required this.textInputType,
      required this.autofillHints,
      this.onSubmitted,
      this.obscured = false});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          text,
          style: Theme.of(context).textTheme.subtitle2?.copyWith(
                fontWeight: FontWeight.bold,
              ),
        ),
        const SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kBoxDecorationStyle(context),
          padding: EdgeInsets.zero,
          height: 60.0,
          child: Center(
            child: TextField(
                onChanged: (value) => onPressed(context, value),
                onSubmitted: (String value) {
                  if (onSubmitted != null) {
                    onSubmitted!();
                  }
                },
                autofillHints: autofillHints,
                keyboardType: textInputType,
                style: Theme.of(context)
                    .textTheme
                    .bodyText2
                    ?.copyWith(fontSize: 18.0),
                obscureText: obscured,
                decoration: kInputDecoration(context).copyWith(
                  contentPadding: const EdgeInsets.fromLTRB(12, 22, 20, 22),
                  prefixIcon: Icon(icon),
                  hintText: 'Enter your $text',
                  hintStyle: Theme.of(context).textTheme.bodyText2,
                )),
          ),
        ),
      ],
    );
  }
}

class EmailField extends StatelessWidget {
  void updateEmail(BuildContext context, String email) {
    context.read(emailProvider).state = email;
  }

  @override
  Widget build(BuildContext context) {
    return _LoginTextField(
      text: 'Email',
      onPressed: updateEmail,
      icon: Icons.email,
      textInputType: TextInputType.emailAddress,
      autofillHints: const [
        AutofillHints.username,
        AutofillHints.newUsername,
        AutofillHints.email
      ],
    );
  }
}

class PasswordField extends HookWidget {
  void updatePassword(BuildContext context, String pass) {
    context.read(passwordProvider).state = pass;
  }

  @override
  Widget build(BuildContext context) {
    final email = useProvider(emailProvider).state;
    final pass = useProvider(passwordProvider).state;
    final _auth = useProvider(authControllerProvider);
    return _LoginTextField(
      text: 'Password',
      onPressed: updatePassword,
      icon: Icons.lock,
      textInputType: TextInputType.visiblePassword,
      autofillHints: const [AutofillHints.password, AutofillHints.newPassword],
      obscured: true,
      onSubmitted: () => authCall(
        context: context,
        authFunction: _auth.signIn,
        email: email,
        pass: pass,
      ),
    );
  }
}
