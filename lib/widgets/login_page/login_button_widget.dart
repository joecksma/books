import 'package:books/models/failures/auth_failure.dart';
import 'package:books/providers/auth_provider.dart';
import 'package:books/providers/login_page_providers.dart';
import 'package:books/widgets/login_page/sign_in_function.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class _BuildButton extends HookWidget {
  final String text;
  final Color buttonColor;
  final Color textColor;
  final bool outline;

  final Future<Either<AuthFailure, String>> Function({
    required String email,
    required String password,
  }) onPressed;

  const _BuildButton({
    required this.text,
    required this.onPressed,
    required this.buttonColor,
    required this.textColor,
    required this.outline,
  });

  @override
  Widget build(BuildContext context) {
    final email = useProvider(emailProvider).state;
    final pass = useProvider(passwordProvider).state;
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 25.0),
      width: double.infinity,
      child: MaterialButton(
        elevation: 5.0,
        onPressed: () => authCall(
          context: context,
          authFunction: onPressed,
          email: email,
          pass: pass,
        ),
        padding: const EdgeInsets.all(15.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30.0),
          side: outline
              ? BorderSide(color: Theme.of(context).colorScheme.onBackground)
              : BorderSide.none,
        ),
        color: buttonColor,
        child: Text(
          text,
          style: Theme.of(context).textTheme.button?.copyWith(
                letterSpacing: 1.5,
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
                fontFamily: 'OpenSans',
                color: textColor,
              ),
        ),
      ),
    );
  }
}

class LoginButton extends HookWidget {
  @override
  Widget build(BuildContext context) {
    final _auth = useProvider(authControllerProvider);
    return _BuildButton(
      buttonColor: Theme.of(context).primaryColor,
      textColor: Theme.of(context).colorScheme.onPrimary,
      outline: false,
      text: 'LOGIN',
      onPressed: _auth.signIn,
    );
  }
}

class SignUpButton extends HookWidget {
  @override
  Widget build(BuildContext context) {
    final _auth = useProvider(authControllerProvider);
    return _BuildButton(
      buttonColor: Theme.of(context).backgroundColor,
      textColor: Theme.of(context).colorScheme.onBackground,
      outline: true,
      text: 'SIGN-UP',
      onPressed: _auth.signUp,
    );
  }
}
