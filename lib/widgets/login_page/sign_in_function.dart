import 'package:another_flushbar/flushbar_helper.dart';
import 'package:books/models/failures/auth_failure.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';

Future<void> authCall({
  required BuildContext context,
  required Future<Either<AuthFailure, String>> Function({
    required String email,
    required String password,
  })
      authFunction,
  String? email,
  String? pass,
}) async {
  final FocusScopeNode currentFocus = FocusScope.of(context);
  if (!currentFocus.hasPrimaryFocus) {
    currentFocus.unfocus();
  }
  Either<AuthFailure, String> messageOrFailure;
  if (email != null && email.isNotEmpty && pass != null && pass.isNotEmpty) {
    messageOrFailure = await authFunction(email: email, password: pass);
  } else {
    messageOrFailure =
        left(AuthFailure(message: "Please input your Email and Password"));
  }
  messageOrFailure.fold(
    (authFailure) =>
        FlushbarHelper.createError(message: authFailure.message)..show(context),
    (message) => FlushbarHelper.createSuccess(message: message)..show(context),
  );
}
