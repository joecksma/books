import 'package:books/controllers/book_controller.dart';
import 'package:books/models/book/book_model.dart';
import 'package:books/providers/filter_provider.dart';
import 'package:books/widgets/home_page/book_widget/book_widget.dart';
import 'package:books/widgets/home_page/edit_dialog_textfield_widget.dart';
import 'package:books/widgets/home_page/edit_dialog_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class EditTagsDialog extends HookWidget {
  final Book book;
  const EditTagsDialog({Key? key, required this.book}) : super(key: key);

  static void show(BuildContext context, Book book) {
    Navigator.of(context).push(MaterialPageRoute(
      builder: (BuildContext context) {
        return EditTagsDialog(book: book);
      },
      fullscreenDialog: true,
    ));
  }

  @override
  Widget build(BuildContext context) {
    final availableFilter = useProvider(availableFilterProvider).state;
    final tags = useState(book.lists.toSet());
    final bookController = useProvider(bookListControllerProvider.notifier);

    return EditDialog(
      heroTag: 'hero-edit-tags-${book.id}',
      title: 'Edit Tags',
      onPressed: () {
        bookController.updateBook(
          updatedBook: book.copyWith(
            lists: tags.value.toList(),
            updatedAt: DateTime.now(),
          ),
        );
      },
      confirmIcon: Icons.save,
      confirmText: 'SAVE',
      children: [
        BookWidget(book: book),
        EditDialogListField(
          hint: 'Tag',
          values: tags.value,
          knownPossibleValues: availableFilter.tags.include,
          onChanged: (Set<String> values) {
            tags.value = values;
          },
        ),
      ],
    );
  }
}
