import 'package:books/shared/constants.dart';
import 'package:flutter/material.dart';

class EditBookTextField extends StatelessWidget {
  final TextEditingController controller;
  final String hint;
  final int lines;
  final TextInputType type;
  const EditBookTextField(
      {Key? key,
      required this.controller,
      required this.hint,
      this.type = TextInputType.text,
      this.lines = 1})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          hint,
          style: Theme.of(context).textTheme.subtitle2?.copyWith(
                fontWeight: FontWeight.bold,
              ),
        ),
        const SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          // height: 60.0,
          decoration: kBoxDecorationStyle(context),
          padding: EdgeInsets.zero,
          child: TextField(
            controller: controller,
            maxLines: lines,
            keyboardType: type,
            decoration: kInputDecoration(context).copyWith(
              hintText: hint,
              hintStyle: Theme.of(context).textTheme.bodyText2,
            ),
          ),
        ),
        const SizedBox(
          height: 20.0,
        ),
      ],
    );
  }
}
