import 'package:books/controllers/book_controller.dart';
import 'package:books/models/book/book_model.dart';
import 'package:books/providers/filter_provider.dart';
import 'package:books/widgets/dialog_widget.dart';
import 'package:books/widgets/home_page/edit_book_dialog/edit_book_dialog_list_widget.dart';
import 'package:books/widgets/home_page/edit_dialog_textfield_widget.dart';
import 'package:books/widgets/home_page/edit_dialog_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class EditBookDialog extends HookWidget {
  final Book book;

  const EditBookDialog({Key? key, required this.book}) : super(key: key);

  bool get isUpdating => book.id != null;

  static void show(BuildContext context, Book book) {
    Navigator.of(context).push(MaterialPageRoute(
      builder: (BuildContext context) {
        return EditBookDialog(book: book);
      },
      fullscreenDialog: true,
    ));
  }

  @override
  Widget build(BuildContext context) {
    final availableFilter = useProvider(availableFilterProvider).state;
    final titleController = useTextEditingController(text: book.title);
    final authorController = useTextEditingController(text: book.author);
    final descriptionController =
        useTextEditingController(text: book.description);
    final lengthController = useTextEditingController(
        text: book.length > 0 ? book.length.toString() : '');
    final ratingController = useTextEditingController(text: book.rating);
    final urlController = useTextEditingController(text: book.url);
    final characters = useState(book.characters.toSet());
    final genres = useState(book.genre.toSet());

    final bookController = useProvider(bookListControllerProvider.notifier);

    //ignore: prefer_function_declarations_over_variables
    final Book Function(Book) bookCopy = (Book book) => book.copyWith(
          title: titleController.text.trim(),
          author: authorController.text.trim(),
          description: descriptionController.text.trim(),
          length: int.parse(lengthController.text.trim()),
          rating: ratingController.text.trim(),
          url: urlController.text.trim(),
          characters: characters.value.map((e) => e.trim()).toList(),
          genre: genres.value.map((e) => e.trim()).toList(),
          updatedAt: DateTime.now(),
        );

    // return Dialog(
    return EditDialog(
      heroTag: isUpdating ? 'hero-edit-book-${book.id}' : 'hero-add-book',
      title: isUpdating ? 'Edit Book' : 'Add Book',
      confirmIcon: isUpdating ? Icons.update : Icons.save,
      confirmText: isUpdating ? 'UPDATE' : 'SAVE',
      onPressed: () {
        isUpdating
            ? bookController.updateBook(updatedBook: bookCopy(book))
            : bookController.addBook(book: bookCopy(Book.empty()));
      },
      children: [
        EditBookTextField(
          controller: titleController,
          hint: 'Title',
        ),
        EditBookTextField(
          controller: authorController,
          hint: 'Author',
        ),
        EditBookTextField(
          controller: descriptionController,
          hint: 'Description',
          lines: 5,
        ),
        EditDialogListField(
          hint: 'Character',
          values: characters.value,
          knownPossibleValues: availableFilter.characters.include,
          onChanged: (Set<String> values) {
            characters.value = values;
          },
        ),
        EditDialogListField(
          hint: 'Genre',
          values: genres.value,
          knownPossibleValues: availableFilter.genres.include,
          onChanged: (Set<String> values) {
            genres.value = values;
          },
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.min,
          children: [
            Expanded(
              child: EditBookTextField(
                controller: ratingController,
                hint: 'Rating',
              ),
            ),
            const SizedBox(width: 40.0),
            Expanded(
              child: EditBookTextField(
                controller: lengthController,
                hint: 'Length',
                type: TextInputType.number,
              ),
            ),
          ],
        ),
        EditBookTextField(
          controller: urlController,
          hint: 'Url',
          type: TextInputType.url,
        ),
        if (isUpdating)
          Consumer(
            builder: (context, watch, child) {
              final bookController = watch(bookListControllerProvider.notifier);
              return ElevatedButton.icon(
                onPressed: () {
                  DialogWidget.show(
                    context: context,
                    title: 'Delete: ${book.title} by ${book.author}?',
                    onPressed: () {
                      bookController.deleteBook(bookId: book.id!);
                      Navigator.pop(context);
                    },
                  );
                },
                icon: const Icon(Icons.delete),
                label: const Text('Delete'),
              );
            },
          )
        else
          Container(),
      ],
    );
  }
}
