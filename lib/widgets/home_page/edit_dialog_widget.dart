import 'package:flutter/material.dart';

class EditDialog extends StatelessWidget {
  final String heroTag;
  final String title;
  final Function? onPressed;
  final IconData? confirmIcon;
  final String? confirmText;
  final List<Widget>? children;
  final Widget? child;

  const EditDialog({
    Key? key,
    required this.heroTag,
    required this.title,
    this.children,
    this.onPressed,
    this.confirmIcon,
    this.confirmText,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // only allow children or child to be set
    assert((children != null || child != null) &&
        (children == null || child == null));
    return Hero(
      tag: heroTag,
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            title,
            style: TextStyle(
              color: Theme.of(context).colorScheme.onPrimary,
            ),
          ),
          actions: (confirmIcon != null &&
                  confirmText != null &&
                  onPressed != null)
              ? [
                  MaterialButton(
                    onPressed: () {
                      onPressed!();
                      Navigator.of(context).pop();
                    },
                    child: Row(
                      children: [
                        Icon(
                          confirmIcon,
                          color: Theme.of(context).colorScheme.onPrimary,
                        ),
                        const SizedBox(
                          width: 10.0,
                        ),
                        Text(
                          confirmText!,
                          style: TextStyle(
                              color: Theme.of(context).colorScheme.onPrimary),
                        ),
                        const SizedBox(
                          width: 10.0,
                        ),
                      ],
                    ),
                  )
                ]
              : [],
        ),
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: child ??
                SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: children!,
                  ),
                ),
          ),
        ),
      ),
    );
  }
}
