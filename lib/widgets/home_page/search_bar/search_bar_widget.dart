import 'package:books/controllers/filter_controller.dart';
import 'package:books/hooks/floating_search_bar_controller_hook.dart';
import 'package:books/models/book/book_model.dart';
import 'package:books/providers/filter_provider.dart';
import 'package:books/providers/filtered_book_list_provider.dart';
import 'package:books/widgets/dialog_widget.dart';
import 'package:books/widgets/home_page/edit_book_dialog/edit_book_dialog_widget.dart';
import 'package:books/widgets/home_page/search_bar/filter/filter_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:keyboard_service/keyboard_service.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:material_floating_search_bar/material_floating_search_bar.dart';

class SearchBarWidget extends HookWidget {
  SearchBarWidget({
    Key? key,
  }) : super(key: key);

  final FloatingSearchBarController searchController =
      useFloatingSearchBarController();
  final showCloseButtonState = useState(false);

  final searchProvider = useProvider(bookSearchProvider);

  final filter = useProvider(filterControllerProvider);
  final filterController = useProvider(filterControllerProvider.notifier);
  final anyFilterActive = useProvider(anyFilterActiveProvider).state;
  final sortActive = useProvider(sortActiveProvider).state;

  final filteredBooks = useProvider(filteredBookListProvider);

  @override
  Widget build(BuildContext context) {
    return FloatingSearchBar(
      backgroundColor: Theme.of(context).cardColor,
      automaticallyImplyBackButton: false,
      controller: searchController,
      hint: filter.status.isEmpty
          ? 'Search ${filteredBooks.state.length} books...'
          : 'Search ${filteredBooks.state.length} ${filter.status.first.toLowerCase()} books...',
      transitionDuration: const Duration(milliseconds: 200),
      borderRadius: BorderRadius.circular(30.0),
      scrollPadding: const EdgeInsets.only(top: 5.0),
      clearQueryOnClose: false,
      onFocusChanged: (focus) async {
        if (!focus) {
          KeyboardService.dismiss(unfocus: context);
        }
      },
      onQueryChanged: (query) {
        searchProvider.state = query;
        showCloseButtonState.value = query.isNotEmpty;
      },
      actions: [
        FloatingSearchBarAction.icon(
          showIfClosed: sortActive,
          showIfOpened: sortActive,
          icon: const Icon(MdiIcons.sort),
          onTap: () {
            DialogWidget.showNoYesDefault(
              context: context,
              title: 'Reset sort?',
              onPressed: () => filterController.resetSort(),
              onPressedDefault: () => filterController.resetToSavedSort(),
            );
          },
        ),
        FloatingSearchBarAction.icon(
          showIfClosed: anyFilterActive,
          showIfOpened: anyFilterActive,
          icon: const Icon(MdiIcons.filter),
          onTap: () {
            DialogWidget.showNoYesDefault(
              context: context,
              title: 'Clear Filter?',
              onPressed: () => filterController.clearFilter(),
              onPressedDefault: () => filterController.resetToSavedFilter(),
            );
          },
        ),
        FloatingSearchBarAction.icon(
          showIfOpened: true,
          showIfClosed: showCloseButtonState.value,
          icon: const Icon(Icons.close),
          onTap: () {
            if (searchController.query.isEmpty) {
              KeyboardService.dismiss(unfocus: context);
            }
            searchController.clear();
          },
        ),
        FloatingSearchBarAction(
          child: Hero(
            tag: 'hero-add-book',
            child: CircularButton(
              icon: const Icon(MdiIcons.plus),
              onPressed: () {
                EditBookDialog.show(context, Book.empty());
              },
            ),
          ),
        ),
      ],
      builder: (context, transition) {
        return SearchBarFilter();
      },
    );
  }
}
