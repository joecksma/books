import 'package:books/widgets/home_page/search_bar/filter/collapsible_filter_widget.dart';
import 'package:flutter/material.dart';
import 'package:number_display/number_display.dart';

class CollapsibleRangeFilter extends StatelessWidget {
  final String label;
  final IconData icon;
  final RangeValues possibleRange;
  final RangeValues filteredRange;
  final Function(RangeValues) onChanged;

  const CollapsibleRangeFilter({
    Key? key,
    required this.label,
    required this.icon,
    required this.possibleRange,
    required this.filteredRange,
    required this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final display = createDisplay(length: 4, decimal: 2);
    // fix range in ui
    double minLength = filteredRange.start == double.infinity ||
            filteredRange.start < possibleRange.start
        ? possibleRange.start
        : filteredRange.start;
    double maxLength = filteredRange.end == double.negativeInfinity ||
            filteredRange.end > possibleRange.end
        ? possibleRange.end
        : filteredRange.end;
    if (minLength > maxLength) {
      minLength = possibleRange.start;
      maxLength = possibleRange.end;
    }
    final range = RangeValues(minLength, maxLength);
    final int divisions = (possibleRange.end - possibleRange.start) ~/ 10000;
    return CollapsibleFilter(
      active: possibleRange != range,
      icon: icon,
      label: label,
      child: Padding(
        padding: const EdgeInsets.only(bottom: 4),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(display(range.start)),
            Expanded(
              child: RangeSlider(
                values: range,
                min: possibleRange.start,
                max: possibleRange.end,
                divisions: divisions >= 1 ? divisions : 1,
                labels: RangeLabels(
                  '${range.start ~/ 1000}k',
                  '${range.end ~/ 1000}k',
                ),
                onChanged: (RangeValues values) => onChanged(values),
              ),
            ),
            Text(display(range.end)),
          ],
        ),
      ),
    );
  }
}
