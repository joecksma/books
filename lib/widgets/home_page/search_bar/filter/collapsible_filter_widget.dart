import 'package:collapsible/collapsible.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:keyboard_service/keyboard_service.dart';

class CollapsibleFilter extends HookWidget {
  final String label;
  final IconData icon;
  final Widget child;
  final bool active;

  const CollapsibleFilter({
    Key? key,
    required this.label,
    required this.icon,
    required this.active,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final collapsed = useState(true);
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(30),
      ),
      margin: const EdgeInsets.symmetric(vertical: 4),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          ListTile(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30),
            ),
            onTap: () {
              collapsed.value = !collapsed.value;
              KeyboardService.dismiss(unfocus: context);
            },
            dense: true,
            leading: Icon(
              icon,
              color: active
                  ? Theme.of(context).primaryColor
                  : Theme.of(context).colorScheme.onBackground,
            ),
            title: Text(
              label,
              style: Theme.of(context).textTheme.headline6,
            ),
            trailing: Icon(
              collapsed.value ? Icons.expand_more : Icons.expand_less,
            ),
          ),
          Collapsible(
            collapsed: collapsed.value,
            maintainState: true,
            axis: CollapsibleAxis.vertical,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: child,
            ),
          ),
        ],
      ),
    );
  }
}
