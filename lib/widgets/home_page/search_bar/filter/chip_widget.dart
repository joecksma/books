import 'package:flutter/material.dart';

class SearchFilterChip extends StatelessWidget {
  final String label;
  final Function onPressed;
  final bool active;
  final IconData? checkIcon;
  const SearchFilterChip({
    Key? key,
    required this.label,
    required this.onPressed,
    required this.active,
    this.checkIcon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FilterChip(
      avatar: active
          ? Icon(
              checkIcon ?? Icons.check,
              color: active
                  ? Theme.of(context).colorScheme.onPrimary
                  : Theme.of(context).colorScheme.onBackground,
            )
          : null,
      showCheckmark: false,
      shape: StadiumBorder(
        side: BorderSide(
          color: active
              ? Theme.of(context).primaryColor
              : Theme.of(context).colorScheme.onBackground,
          width: 0.0,
        ),
      ),
      selected: active,
      onSelected: (bool value) {
        onPressed();
      },
      selectedColor: Theme.of(context).primaryColor,
      label: Text(
        label,
        style: TextStyle(
          color: active
              ? Theme.of(context).colorScheme.onPrimary
              : Theme.of(context).colorScheme.onBackground,
        ),
      ),
    );
  }
}
