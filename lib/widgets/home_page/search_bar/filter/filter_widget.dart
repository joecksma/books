import 'package:books/controllers/filter_controller.dart';
import 'package:books/models/filter/filter_list_model.dart';
import 'package:books/providers/filter_provider.dart';
import 'package:books/shared/enums.dart';
import 'package:books/widgets/home_page/search_bar/filter/collapsible_list_filter_widget.dart';
import 'package:books/widgets/home_page/search_bar/filter/collapsible_range_filter_widget.dart';
import 'package:books/widgets/home_page/search_bar/filter/filter_save_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class SearchBarFilter extends HookWidget {
  @override
  Widget build(BuildContext context) {
    final filterController = useProvider(filterControllerProvider.notifier);
    final filter = useProvider(filterControllerProvider);
    final available = useProvider(availableFilterProvider).state;

    return Column(
      children: [
        CollapsibleListFilter(
          label: 'Status',
          icon: MdiIcons.listStatus,
          possibleValues: available.status,
          filter: FilterList(include: filter.status, exclude: {}, and: false),
          showAnd: false,
          showExclude: false,
          onPressed: (value, exclude) {
            // Allow only one to be active
            if (filter.status.contains(value)) {
              filterController.setFilter(filter: filter.copyWith(status: {}));
            } else {
              filterController.setFilter(
                  filter: filter.copyWith(status: {value}));
            }
          },
          onChanged: (FilterList value) {
            filterController.setFilter(
                filter: filter.copyWith(status: value.include));
          },
        ),
        CollapsibleListFilter(
          label: 'Sort',
          icon: MdiIcons.sort,
          possibleValues: available.sort.include,
          filter: filter.sort,
          showAnd: false,
          showExclude: false,
          activeOverwrite:
              filter.sort.include.first != sortCreationTime || !filter.sort.and,
          rightWidget: TextButton(
            onPressed: () {
              filterController.setFilter(
                  filter: filter.copyWith(
                      sort: filter.sort.copyWith(and: !filter.sort.and)));
            },
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  filter.sort.and
                      ? MdiIcons.sortDescending
                      : MdiIcons.sortAscending,
                  color: filter.sort.and
                      ? Theme.of(context).colorScheme.onBackground
                      : Theme.of(context).colorScheme.primary,
                ),
                Text(
                  filter.sort.and ? 'Desc.' : 'Asc. ',
                  style: TextStyle(
                    color: filter.sort.and
                        ? Theme.of(context).colorScheme.onBackground
                        : Theme.of(context).colorScheme.primary,
                  ),
                ),
              ],
            ),
          ),
          onPressed: (value, exclude) {
            // Allow only one to be active
            filterController.setFilter(
              filter: filter.copyWith(
                sort: filter.sort.copyWith(include: {value}),
              ),
            );
          },
          onChanged: (FilterList value) {
            filterController.setFilter(filter: filter.copyWith(sort: value));
          },
        ),
        CollapsibleListFilter(
          icon: Icons.shield,
          label: 'Rating',
          possibleValues: available.ratings.include,
          filter: filter.ratings,
          showAnd: false,
          showExclude: false,
          onChanged: (FilterList values) {
            filterController.setFilter(
              filter: filter.copyWith(ratings: values),
            );
          },
        ),
        CollapsibleRangeFilter(
          icon: Icons.text_fields,
          label: 'Length',
          filteredRange: RangeValues(filter.minLength, filter.maxLength),
          possibleRange: RangeValues(available.minLength, available.maxLength),
          onChanged: (RangeValues value) {
            filterController.setFilter(
              filter: filter.copyWith(
                minLength: value.start,
                maxLength: value.end,
              ),
            );
          },
        ),
        CollapsibleListFilter(
          label: 'Tags',
          icon: Icons.tag,
          possibleValues: available.tags.include,
          filter: filter.tags,
          onChanged: (FilterList value) {
            filterController.setFilter(filter: filter.copyWith(tags: value));
          },
        ),
        CollapsibleListFilter(
          icon: Icons.category,
          label: 'Genres',
          possibleValues: available.genres.include,
          filter: filter.genres,
          onChanged: (FilterList value) {
            filterController.setFilter(filter: filter.copyWith(genres: value));
          },
        ),
        CollapsibleListFilter(
          icon: Icons.people,
          label: 'Characters',
          possibleValues: available.characters.include,
          filter: filter.characters,
          onChanged: (FilterList value) {
            filterController.setFilter(
              filter: filter.copyWith(characters: value),
            );
          },
        ),
        CollapsibleFilterSave(),
      ],
    );
  }
}
