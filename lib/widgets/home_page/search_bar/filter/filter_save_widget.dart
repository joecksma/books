import 'package:books/controllers/filter_controller.dart';
import 'package:books/models/filter/filter_list_model.dart';
import 'package:books/models/filter/filter_model.dart';
import 'package:books/widgets/home_page/search_bar/filter/collapsible_list_filter_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class CollapsibleFilterSave extends HookWidget {
  final filterController = useProvider(filterControllerProvider.notifier);
  final filter = useProvider(filterControllerProvider);
  //TODO: Refactor to use a provider instead of local state?
  final save = useState(const FilterList());
  final possible = {
    'Status',
    'Sort',
    'Rating',
    'Length',
    'Tags',
    'Genres',
    'Characters'
  };

  @override
  Widget build(BuildContext context) {
    return CollapsibleListFilter(
      label: 'Save as Default',
      icon: Icons.save,
      possibleValues: possible,
      filter: save.value,
      showAnd: false,
      showExclude: false,
      activeOverwrite: false,
      rightWidget: TextButton(
        onPressed: () async {
          final saved = await filterController.getSavedFilter();
          filterController.saveFilter(
            filter: Filter(
              status: save.value.include.contains('Status')
                  ? filter.status
                  : saved.status,
              sort: save.value.include.contains('Sort')
                  ? filter.sort
                  : saved.sort,
              minLength: save.value.include.contains('Length')
                  ? filter.minLength
                  : saved.minLength,
              maxLength: save.value.include.contains('Length')
                  ? filter.maxLength
                  : saved.maxLength,
              ratings: save.value.include.contains('Rating')
                  ? filter.ratings
                  : saved.ratings,
              tags: save.value.include.contains('Tags')
                  ? filter.tags
                  : saved.tags,
              characters: save.value.include.contains('Characters')
                  ? filter.characters
                  : saved.characters,
              genres: save.value.include.contains('Genres')
                  ? filter.genres
                  : saved.genres,
            ),
          );
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              Icons.save,
              color: Theme.of(context).colorScheme.onBackground,
            ),
            Text(
              'Save',
              style:
                  TextStyle(color: Theme.of(context).colorScheme.onBackground),
            ),
          ],
        ),
      ),
      onChanged: (FilterList value) {
        save.value = value;
      },
    );
  }
}
