import 'package:books/models/filter/filter_list_model.dart';
import 'package:books/shared/constants.dart';
import 'package:books/widgets/home_page/search_bar/filter/chip_widget.dart';
import 'package:books/widgets/home_page/search_bar/filter/collapsible_filter_widget.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class CollapsibleListFilter extends HookWidget {
  final String label;
  final IconData icon;
  final Set<String> possibleValues;
  final FilterList filter;
  final Function(String, bool)? onPressed;
  final bool? activeOverwrite;
  final bool showExclude;
  final bool showAnd;
  final Widget? rightWidget;
  final Function(FilterList) onChanged;

  const CollapsibleListFilter({
    Key? key,
    required this.label,
    required this.icon,
    required this.possibleValues,
    required this.filter,
    required this.onChanged,
    this.rightWidget,
    this.onPressed,
    this.activeOverwrite,
    this.showAnd = true,
    this.showExclude = true,
  }) : super(key: key);

  Set<String> _search(Set<String> values, String search) {
    if (search.isEmpty) {
      return values;
    }
    final tokens = search.toLowerCase().split(',');
    return values.where((value) {
      return tokens.any((token) {
        return value.toLowerCase().contains(token);
      });
    }).toSet();
  }

  Widget _excludeButton(ValueNotifier<bool> excludeActive) {
    if (showExclude) {
      return IconButton(
        padding: EdgeInsets.zero,
        onPressed: () {
          excludeActive.value = !excludeActive.value;
        },
        icon: Icon(
          excludeActive.value ? MdiIcons.filterRemove : MdiIcons.filterPlus,
        ),
      );
    } else {
      return Container();
    }
  }

  List<Widget> _listToChips(BuildContext context, Set<String> possible,
      Set<String> values, Set<String> exclude, bool showExclude) {
    if (possible.isEmpty && values.isEmpty && exclude.isEmpty) {
      return [
        Center(
          child: Text(
            '🤷 Nothing here',
            style: Theme.of(context).textTheme.headline5,
          ),
        ),
      ];
    }
    final display = [
      ...values.map((v) => {'value': v, 'type': 'include'}),
      ...exclude.map((v) => {'value': v, 'type': 'exclude'}),
      ...possible.toSet().difference({...values, ...exclude}).map(
          (v) => {'value': v, 'type': 'possible'})
    ];

    display.sort((Map<String, String> a, Map<String, String> b) {
      return (a['value']!).compareTo(b['value']!);
    });
    return display
        .map(
          (value) => SearchFilterChip(
            label: value['value']!,
            checkIcon: value['type']! == 'exclude' ? Icons.close : null,
            active: ['exclude', 'include'].contains(value['type']),
            onPressed: () {
              if (onPressed != null) {
                onPressed!(value['value']!, showExclude);
              } else {
                final bool removeFilter =
                    filter.include.contains(value['value']);
                final bool removeExclude =
                    filter.exclude.contains(value['value']);
                final bool addFilter =
                    !removeExclude && !removeFilter && !showExclude;
                final bool addExclude =
                    !removeExclude && !removeFilter && showExclude;

                if (removeFilter) {
                  onChanged(
                    filter.copyWith(
                      include: filter.include
                          .where((v) => v != value['value']!)
                          .toSet(),
                    ),
                  );
                }
                if (removeExclude) {
                  onChanged(
                    filter.copyWith(
                        exclude: filter.exclude
                            .where((v) => v != value['value']!)
                            .toSet()),
                  );
                }
                if (addFilter) {
                  onChanged(filter
                      .copyWith(include: {...filter.include, value['value']!}));
                }
                if (addExclude) {
                  onChanged(filter
                      .copyWith(exclude: {...filter.exclude, value['value']!}));
                }
              }
            },
          ),
        )
        .toList();
  }

  Widget _buildChipWrap(BuildContext context, bool initial, bool excludeActive,
      Set<String> searchResultChips) {
    return Expanded(
      child: Wrap(
        runSpacing: kIsWeb ? 16 : 0,
        spacing: 6,
        runAlignment: WrapAlignment.spaceEvenly,
        alignment: WrapAlignment.spaceEvenly,
        children: _listToChips(
          context,
          initial ? possibleValues : searchResultChips,
          filter.include,
          showExclude ? filter.exclude : {},
          excludeActive,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final controller = useTextEditingController();
    final searchResultChips = useState({...possibleValues, ...filter.include});
    final excludeActive = useState(false);
    final initial = useState(true);
    return CollapsibleFilter(
      active: activeOverwrite ??
          filter.include.isNotEmpty ||
              (showExclude && filter.exclude.isNotEmpty),
      icon: icon,
      label: label,
      child: Column(
        children: [
          if (showAnd) ...[
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Expanded(
                  child: Container(
                    height: 30,
                    decoration: kBoxDecorationStyle(context),
                    child: TextField(
                      controller: controller,
                      decoration: kInputDecoration(context).copyWith(
                        contentPadding: const EdgeInsets.symmetric(
                          vertical: 10,
                          horizontal: 20,
                        ),
                        suffixIcon: controller.text.isNotEmpty
                            ? IconButton(
                                icon: const Icon(Icons.clear, size: 16),
                                onPressed: () {
                                  controller.clear();
                                  searchResultChips.value = {
                                    ...possibleValues,
                                    ...(excludeActive.value && showExclude)
                                        ? filter.exclude
                                        : filter.include
                                  };
                                })
                            : const SizedBox(width: 16, height: 16),
                        hintText: 'Search...',
                        hintStyle: Theme.of(context).textTheme.bodyText2,
                        isDense: true,
                      ),
                      onChanged: (text) {
                        searchResultChips.value = _search({
                          ...possibleValues,
                          ...(excludeActive.value && showExclude)
                              ? filter.exclude
                              : filter.include
                        }, text.trim());
                        initial.value = false;
                      },
                    ),
                  ),
                ),
                IconButton(
                  padding: EdgeInsets.zero,
                  onPressed: () => onChanged(filter.copyWith(and: !filter.and)),
                  icon: Icon(filter.and
                      ? MdiIcons.multiplicationBox
                      : MdiIcons.numeric1Box),
                ),
                if (showExclude) _excludeButton(excludeActive),
              ],
            ),
          ] else if (showExclude)
            _excludeButton(excludeActive),
          const SizedBox(
            height: kIsWeb ? 10 : 0,
          ),
          if (rightWidget != null)
            IntrinsicHeight(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  _buildChipWrap(context, initial.value, excludeActive.value,
                      searchResultChips.value),
                  const VerticalDivider(),
                  rightWidget!,
                ],
              ),
            )
          else
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                _buildChipWrap(context, initial.value, excludeActive.value,
                    searchResultChips.value),
              ],
            ),
          const SizedBox(
            height: kIsWeb ? 16.0 : 8.0,
          )
        ],
      ),
    );
  }
}
