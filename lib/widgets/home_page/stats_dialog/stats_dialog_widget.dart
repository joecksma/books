import 'package:books/providers/filter_provider.dart';
import 'package:books/providers/filtered_book_list_provider.dart';
import 'package:books/widgets/home_page/edit_dialog_widget.dart';
import 'package:books/widgets/home_page/stats_dialog/stats_card_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:number_display/number_display.dart';

class StatsDialog extends HookWidget {
  const StatsDialog({Key? key}) : super(key: key);

  static void show(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(
      builder: (BuildContext context) {
        return const StatsDialog();
      },
      fullscreenDialog: true,
    ));
  }

  @override
  Widget build(BuildContext context) {
    final display = createDisplay(length: 5, decimal: 2);
    final availableFilter = useProvider(availableFilterProvider).state;
    final bookState = useProvider(bookListProvider).state;

    final totalBooks = bookState.length;
    final readBooks = bookState.where((book) => book.read).length;
    final unreadBooks = bookState.where((book) => !book.read).length;

    final totalWords =
        bookState.fold<int>(0, (prev, book) => prev + book.length);
    final readWords = bookState
        .where((book) => book.read)
        .fold<int>(0, (prev, book) => prev + book.length);
    final unreadWords = bookState
        .where((book) => !book.read)
        .fold<int>(0, (prev, book) => prev + book.length);

    final booksPerRating = availableFilter.ratings.include
        .map((rating) =>
            "${rating.padRight(2)} ${bookState.where((book) => book.rating == rating).length}"
                .padRight(7))
        .toList();

    final authors = bookState.map((book) => book.author).toSet().length;

    return EditDialog(
      heroTag: 'hero-statistics',
      title: 'Statistics',
      child: Column(
        children: [
          Card(
            child: Center(
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 12.0),
                child: Text(
                  'Statistics',
                  style: Theme.of(context).textTheme.headline4?.copyWith(
                        color: Theme.of(context).textTheme.headline5?.color,
                        fontWeight: FontWeight.bold,
                      ),
                ),
              ),
            ),
          ),
          const SizedBox(height: 24.0),
          Expanded(
            child: GridView.count(
              crossAxisSpacing: 4,
              mainAxisSpacing: 4,
              crossAxisCount: MediaQuery.of(context).size.width ~/ 200,
              childAspectRatio: 16.0 / 9.0,
              children: [
                StatsCard.oneLine(
                  title: "Books",
                  icon: Icons.book,
                  text: display(totalBooks),
                ),
                StatsCard.twoLine(
                  title: "Read Books",
                  icon: Icons.bookmark_border_rounded,
                  text: display(readBooks),
                  secondText: "( ${display(readBooks / totalBooks * 100)}% )",
                ),
                StatsCard.twoLine(
                  title: "Unread Books",
                  icon: Icons.bookmark_rounded,
                  text: display(unreadBooks),
                  secondText: "( ${display(unreadBooks / totalBooks * 100)}% )",
                ),
                StatsCard.oneLine(
                  title: "Words",
                  icon: Icons.text_fields_rounded,
                  text: display(totalWords),
                ),
                StatsCard.twoLine(
                  title: "Read Words",
                  icon: Icons.spellcheck_rounded,
                  text: display(readWords),
                  secondText: "( ${display(readWords / totalWords * 100)}% )",
                ),
                StatsCard.twoLine(
                  title: "Unread Words",
                  icon: Icons.text_format_rounded,
                  text: display(unreadWords),
                  secondText:
                      "( ${display(unreadWords / totalWords * 100)}% )", // "${display(unreadWords)}\n( ${display(unreadWords / totalWords * 100)}% )",
                ),
                StatsCard.oneLine(
                  title: "Avg. Length",
                  icon: Icons.format_size_rounded,
                  text: display(totalWords / totalBooks),
                ),
                StatsCard.grid(
                  title: "Ratings",
                  icon: Icons.shield_rounded,
                  items: booksPerRating,
                ),
                StatsCard.oneLine(
                  title: "Authors",
                  icon: Icons.person,
                  text: display(authors),
                ),
                StatsCard.oneLine(
                  title: "Tags",
                  icon: Icons.tag,
                  text: display(availableFilter.tags.include.length),
                ),
                StatsCard.oneLine(
                  title: "Genres",
                  icon: Icons.category,
                  text: display(availableFilter.genres.include.length),
                ),
                StatsCard.oneLine(
                  title: "Characters",
                  icon: Icons.people,
                  text: display(availableFilter.characters.include.length),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
