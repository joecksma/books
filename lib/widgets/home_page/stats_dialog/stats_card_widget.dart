import 'dart:ui';

import 'package:flutter/material.dart';

class StatsCard extends StatelessWidget {
  final String title;
  final IconData icon;
  final Widget child;
  final double preChildPadding;

  const StatsCard({
    Key? key,
    required this.title,
    required this.icon,
    required this.child,
    this.preChildPadding = 8,
  }) : super(key: key);

  factory StatsCard.oneLine({
    Key? key,
    required String title,
    required IconData icon,
    required String text,
    double preChildPadding = 8,
  }) =>
      StatsCard(
          key: key,
          title: title,
          icon: icon,
          preChildPadding: preChildPadding,
          child: Text(
            text,
            style: const TextStyle(
              fontFeatures: [FontFeature.tabularFigures()],
            ),
          ));

  factory StatsCard.twoLine({
    Key? key,
    required String title,
    required IconData icon,
    required String text,
    required String secondText,
    double preChildPadding = 8,
  }) =>
      StatsCard(
        key: key,
        title: title,
        icon: icon,
        preChildPadding: preChildPadding,
        child: Column(
          children: [
            Text(
              text,
              style: const TextStyle(
                fontFeatures: [FontFeature.tabularFigures()],
              ),
            ),
            Text(
              secondText,
              style: const TextStyle(
                fontFeatures: [FontFeature.tabularFigures()],
              ),
            )
          ],
        ),
      );

  factory StatsCard.grid({
    Key? key,
    required String title,
    required IconData icon,
    required List<String> items,
    int rows = 2,
    double preChildPadding = 4,
  }) {
    final entries = items.asMap().entries;
    return StatsCard(
      key: key,
      title: title,
      icon: icon,
      preChildPadding: preChildPadding,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          for (var row = 0; row < rows; row++)
            Column(
              children: entries
                  .where((entry) => entry.key % rows == row)
                  .map((entry) => Card(elevation: 0, child: Text(entry.value)))
                  .toList(),
            ),
        ],
      ),
    );
  }
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.only(
          top: 12.0,
          left: 4.0,
          right: 4.0,
          bottom: 4.0,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Stack(
              children: [
                Align(
                  alignment: Alignment.centerLeft,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Icon(
                      icon,
                      size: 20,
                    ),
                  ),
                ),
                Center(
                  child: Text(
                    title,
                    style: Theme.of(context).textTheme.headline6?.copyWith(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                        ),
                  ),
                ),
              ],
            ),
            SizedBox(height: preChildPadding),
            Expanded(child: child),
          ],
        ),
      ),
    );
  }
}
