import 'package:books/controllers/book_controller.dart';
import 'package:books/models/failures/firebase_failure.dart';
import 'package:books/providers/current_book_provider.dart';
import 'package:books/providers/filtered_book_list_provider.dart';
import 'package:books/widgets/home_page/book_list/book_list_error_widget.dart';
import 'package:books/widgets/home_page/book_list/nothing_here_widget.dart';
import 'package:books/widgets/home_page/book_widget/slidable_book_widget.dart';
import 'package:draggable_scrollbar/draggable_scrollbar.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class BookList extends HookWidget {
  const BookList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bookListState = useProvider(bookListControllerProvider);
    final filteredBookList = useProvider(filteredBookListProvider);
    final bookListNotifier = useProvider(bookListControllerProvider.notifier);
    final scrollController = useScrollController();

    return bookListState.when(
      data: (books) {
        return RefreshIndicator(
          onRefresh: () async {
            await bookListNotifier.readBooks(isRefreshing: true);
          },
          child: filteredBookList.state.isEmpty
              ? NothingHere(filtered: books.isNotEmpty)
              : DraggableScrollbar.rrect(
                  backgroundColor: Theme.of(context).colorScheme.primary,
                  controller: scrollController,
                  child: ListView.separated(
                    physics: const AlwaysScrollableScrollPhysics(),
                    separatorBuilder: (BuildContext context, int index) {
                      return const Divider(
                        height: 5,
                        thickness: 2,
                        indent: 15,
                        endIndent: 15,
                      );
                    },
                    itemCount: filteredBookList.state.length,
                    controller: scrollController,
                    itemBuilder: (BuildContext conext, int index) {
                      final book = filteredBookList.state[index];
                      return ProviderScope(
                        overrides: [
                          currentBookProvider.overrideWithValue(book)
                        ],
                        child: const SlidableBookWidget(),
                      );
                    },
                  ),
                ),
        );
      },
      loading: () => const Center(
        child: CircularProgressIndicator(),
      ),
      error: (failure, _) => BookListError(
          message: failure is FirebaseFailure
              ? failure.message
              : 'Something went wrong!'),
    );
  }
}
