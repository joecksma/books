import 'package:books/controllers/book_controller.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class BookListError extends StatelessWidget {
  final String message;
  const BookListError({
    Key? key,
    required this.message,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(message, style: const TextStyle(fontSize: 20.0)),
          const SizedBox(height: 20.0),
          Consumer(builder: (context, watch, child) {
            return ElevatedButton(
              onPressed: () => watch(bookListControllerProvider.notifier)
                  .readBooks(isRefreshing: true),
              child: const Text('Retry'),
            );
          }),
        ],
      ),
    );
  }
}
