import 'package:flutter/material.dart';

class NothingHere extends StatelessWidget {
  final bool filtered;
  const NothingHere({
    Key? key,
    this.filtered = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ListView(
        physics: const AlwaysScrollableScrollPhysics(),
        children: [
          const SizedBox(height: 250.0),
          Center(
            child: Text(
              '🤷',
              style: Theme.of(context).textTheme.headline2,
            ),
          ),
          Center(
            child: Text(
              'Noting here',
              style: Theme.of(context).textTheme.headline4,
            ),
          ),
          const SizedBox(height: 20.0),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              if (filtered) ...[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Clear the',
                      style: Theme.of(context).textTheme.headline5,
                    ),
                    const Icon(
                      Icons.filter_alt_outlined,
                      size: 32.0,
                    ),
                    Text(
                      ' or ',
                      style: Theme.of(context).textTheme.headline5,
                    ),
                    const Icon(
                      Icons.search,
                      size: 32.0,
                    ),
                  ],
                ),
                const SizedBox(height: 20),
                Text(
                  '- OR -',
                  style: Theme.of(context).textTheme.headline6,
                ),
                const SizedBox(height: 20),
              ],
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('Press ', style: Theme.of(context).textTheme.headline5),
                  const Icon(
                    Icons.add,
                    size: 32.0,
                  ),
                  Text('to add a new book',
                      style: Theme.of(context).textTheme.headline5),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
