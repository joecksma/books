import 'package:books/widgets/home_page/drawer/drawer_card_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:theme_provider/theme_provider.dart';

class ThemeSelectorWidget extends HookWidget {
  final String username;

  const ThemeSelectorWidget({Key? key, required this.username})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final String currentThemeId = ThemeProvider.themeOf(context).id;
    return DrawerCard(
      title: 'Theme',
      child: Column(
        children: [
          ...ThemeProvider.controllerOf(context)
              .allThemes
              .map((theme) => _buildThemeTile(context, theme, currentThemeId))
              .toList()
        ],
      ),
    );
  }

  String _capitalize(String s) {
    if (s.isEmpty) {
      return s;
    } else if (s.length == 1) {
      return s.toUpperCase();
    } else {
      return s[0].toUpperCase() + s.substring(1);
    }
  }

  Widget _buildThemeTile(
    BuildContext context,
    AppTheme theme,
    String currentThemeId,
  ) {
    final String themeName = theme.id.split("_").map(_capitalize).join(" ");

    return ListTile(
      leading: Stack(
        children: <Widget>[
          CircleAvatar(
            backgroundColor: theme.data.canvasColor,
            child: CircleAvatar(
              backgroundColor: theme.data.primaryColor,
              radius: 15,
              child: CircleAvatar(
                backgroundColor: theme.data.colorScheme.onPrimary,
                radius: 5,
              ),
            ),
          ),
          AnimatedOpacity(
            duration: const Duration(milliseconds: 200),
            opacity: theme.id == currentThemeId ? 1 : 0,
            child: CircleAvatar(
              backgroundColor: const Color(0x669E9E9E),
              child: Icon(Icons.check,
                  color: Theme.of(context).colorScheme.primaryVariant),
            ),
          ),
        ],
      ),
      title: Text(themeName),
      subtitle: Text(theme.description),
      onTap: () => ThemeProvider.controllerOf(context).setTheme(theme.id),
    );
  }
}
