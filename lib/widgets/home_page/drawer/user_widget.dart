import 'package:books/providers/auth_provider.dart';
import 'package:books/widgets/dialog_widget.dart';
import 'package:books/widgets/home_page/drawer/drawer_card_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class UserWidget extends HookWidget {
  final String username;

  UserWidget({Key? key, required this.username}) : super(key: key);

  final _auth = useProvider(authControllerProvider);

  @override
  Widget build(BuildContext context) {
    return DrawerCard(
      title: 'User',
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Row(
              children: [
                CircleAvatar(
                  backgroundColor: Theme.of(context).primaryColor,
                  foregroundColor: Theme.of(context).colorScheme.onPrimary,
                  child: const Icon(Icons.person),
                ),
                const SizedBox(width: 10.0),
                Text(username),
              ],
            ),
            TextButton.icon(
              onPressed: () {
                DialogWidget.show(
                  context: context,
                  title: 'Logout?',
                  onPressed: () {
                    _auth.signout();
                  },
                );
              },
              icon: const Icon(Icons.logout),
              label: Text(
                'Logout',
                style: Theme.of(context).textTheme.bodyText1,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
