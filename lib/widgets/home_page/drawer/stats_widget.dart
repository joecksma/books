import 'package:books/widgets/home_page/drawer/drawer_card_widget.dart';
import 'package:books/widgets/home_page/stats_dialog/stats_dialog_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class StatsWidget extends HookWidget {
  const StatsWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DrawerCard(
      title: 'Statistics',
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Hero(
              tag: 'hero-statistics',
              child: TextButton.icon(
                onPressed: () {
                  StatsDialog.show(context);
                },
                icon: const Icon(Icons.bar_chart_rounded),
                label: Text(
                  'View Statistics',
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
