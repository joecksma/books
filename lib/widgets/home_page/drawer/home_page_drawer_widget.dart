import 'package:books/providers/auth_provider.dart';
import 'package:books/widgets/home_page/drawer/stats_widget.dart';
import 'package:books/widgets/home_page/drawer/theme_selector_widget.dart';
import 'package:books/widgets/home_page/drawer/user_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class HomePageDrawer extends HookWidget {
  final _authState = useProvider(authStateProvider);

  @override
  Widget build(BuildContext context) {
    String user = 'User not found';
    _authState.whenData((value) => user = value!.email ?? 'No email for user');

    return Drawer(
      child: SafeArea(
        child: Column(
          children: [
            const SizedBox(height: 24),
            UserWidget(username: user),
            const SizedBox(height: 24),
            ThemeSelectorWidget(username: user),
            const SizedBox(height: 24),
            const StatsWidget(),
          ],
        ),
      ),
    );
  }
}
