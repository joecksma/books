import 'package:books/shared/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';

class EditDialogListField extends HookWidget {
  final String hint;
  final TextInputType type;
  final Set<String> values;
  final Set<String> knownPossibleValues;
  final Function(Set<String>) onChanged;
  EditDialogListField({
    Key? key,
    required this.hint,
    required this.values,
    required this.knownPossibleValues,
    required this.onChanged,
    this.type = TextInputType.text,
  }) : super(key: key);

  final controller = useTextEditingController();

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          '${hint}s',
          style: Theme.of(context).textTheme.subtitle2?.copyWith(
                fontWeight: FontWeight.bold,
              ),
        ),
        const SizedBox(height: 10.0),
        Wrap(
          spacing: 10.0,
          children: values
              .map(
                (value) => ActionChip(
                  label: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(value),
                      const SizedBox(width: 8.0),
                      const Icon(Icons.cancel),
                    ],
                  ),
                  onPressed: () {
                    onChanged({...values.where((e) => e != value)});
                  },
                ),
              )
              .toList(),
        ),
        const SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          // height: 60.0,
          decoration: kBoxDecorationStyle(context),
          padding: EdgeInsets.zero,
          child: TypeAheadField(
            textFieldConfiguration: TextFieldConfiguration(
              controller: controller,
              onSubmitted: (text) {
                onChanged({...values, text.trim()});
                controller.clear();
              },
              keyboardType: type,
              decoration: kInputDecoration(context).copyWith(
                hintText: 'Add $hint',
                hintStyle: Theme.of(context).textTheme.bodyText2,
                suffixIcon: IconButton(
                  onPressed: () {
                    onChanged({...values, controller.text.trim()});
                    controller.clear();
                  },
                  color: Theme.of(context).primaryColor,
                  padding: EdgeInsets.zero,
                  icon: const Icon(Icons.add),
                ),
              ),
            ),
            suggestionsCallback: (pattern) => knownPossibleValues.where(
                (value) =>
                    !values.contains(value) &&
                    value.toLowerCase().contains(pattern.toLowerCase())),
            itemBuilder: (context, String suggestion) {
              return ListTile(
                tileColor: Theme.of(context).cardColor,
                title: Text(suggestion),
              );
            },
            onSuggestionSelected: (String suggestion) {
              onChanged({...values, suggestion});
              controller.clear();
            },
          ),
        ),
        const SizedBox(
          height: 20.0,
        ),
      ],
    );
  }
}
