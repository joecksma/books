import 'package:books/models/book/book_model.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:number_display/number_display.dart';

class BookWidget extends StatelessWidget {
  final Book book;
  const BookWidget({Key? key, required this.book}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      key: ValueKey(book.id),
      title: Padding(
        padding: const EdgeInsets.only(bottom: 5.0),
        child: Column(
          children: [
            Text(
              book.title,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.headline5?.copyWith(
                    fontSize: 18,
                  ),
            ),
            Text(
              book.author,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.headline6?.copyWith(
                    fontSize: 14,
                  ),
            ),
          ],
        ),
      ),
      subtitle: Column(
        children: [
          Text(
            book.description,
            textAlign: TextAlign.justify,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Wrap(
              alignment: WrapAlignment.center,
              runSpacing: kIsWeb ? 10 : 0,
              spacing: 5.0,
              children: _buildChips(context, book),
            ),
          ),
        ],
      ),
    );
  }

  List<Widget> _buildChips(BuildContext context, Book book) {
    final display = createDisplay(length: 4, decimal: 2);
    return [
      ...book.lists
          .map(
            (tag) => Chip(
              shape: StadiumBorder(
                side: BorderSide(
                  color: Theme.of(context).colorScheme.onBackground,
                  width: 0.0,
                ),
              ),
              label: Text(
                tag,
                style: const TextStyle(fontSize: 10.0),
              ),
            ),
          )
          .toList(),
      Chip(
        label: Text(
          book.rating,
          style: const TextStyle(color: Colors.black, fontSize: 10.0),
        ),
        backgroundColor: Colors.red[100]?.withAlpha(200),
      ),
      Chip(
        label: Text(
          display(book.length),
          style: const TextStyle(color: Colors.black, fontSize: 10.0),
        ),
        backgroundColor: Colors.blue[100]?.withAlpha(200),
      ),
      ...book.characters
          .map((character) => Chip(
                label: Text(
                  character.toString(),
                  style: const TextStyle(color: Colors.black, fontSize: 10.0),
                ),
                backgroundColor: Colors.yellow[100]?.withAlpha(200),
              ))
          .toList(),
      ...book.genre
          .map((genre) => Chip(
                label: Text(
                  genre.toString(),
                  style: const TextStyle(color: Colors.black, fontSize: 10.0),
                ),
                backgroundColor: Colors.green[100]?.withAlpha(200),
              ))
          .toList(),
    ];
  }
}
