import 'package:another_flushbar/flushbar_helper.dart';
import 'package:books/controllers/book_controller.dart';
import 'package:books/models/book/book_model.dart';
import 'package:books/providers/current_book_provider.dart';
import 'package:books/shared/themes.dart';
import 'package:books/widgets/home_page/book_widget/book_widget.dart';
import 'package:books/widgets/home_page/edit_book_dialog/edit_book_dialog_widget.dart';
import 'package:books/widgets/home_page/edit_tags_dialog/edit_tags_dialog_widget.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:url_launcher/url_launcher.dart';

class SlidableBookWidget extends ConsumerWidget {
  const SlidableBookWidget({Key? key}) : super(key: key);

  void _onTapStatus(BuildContext context, ScopedReader watch, Book book) {
    watch(bookListControllerProvider.notifier).updateBook(
        updatedBook: book.copyWith(
      read: !book.read,
      updatedAt: DateTime.now(),
    ));
    FlushbarHelper.createAction(
      title: book.read ? "Unread" : "Finished",
      message: '${book.title} - ${book.author}',
      button: MaterialButton(
        onPressed: () {
          Navigator.of(context).pop();
          watch(bookListControllerProvider.notifier).updateBook(
              updatedBook: book.copyWith(
            read: book.read,
            updatedAt: DateTime.now(),
          ));
        },
        textColor: Theme.of(context).primaryColor,
        child: const Text('UNDO'),
      ),
    ).show(context);
  }

  // ignore: avoid_void_async
  void _onTapWeb(BuildContext context, Book book) async {
    if (await canLaunch(book.url)) {
      await launch(book.url);
    } else {
      FlushbarHelper.createError(message: 'Opening in Browser failed!')
          .show(context);
    }
  }

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    final book = watch(currentBookProvider);
    return Slidable(
      actionPane: const SlidableDrawerActionPane(),
      actions: [
        IconSlideAction(
            caption: book.read ? 'Unread' : 'Finished',
            color: book.read ? red : green,
            icon: book.read ? Icons.close : Icons.done,
            onTap: () => _onTapStatus(context, watch, book)),
        Hero(
          tag: 'hero-edit-tags-${book.id}',
          child: IconSlideAction(
            caption: 'Edit Tags',
            color: blue,
            icon: Icons.tag,
            onTap: () {
              EditTagsDialog.show(context, book);
            },
          ),
        ),
      ],
      secondaryActions: [
        Hero(
          tag: 'hero-edit-book-${book.id}',
          child: IconSlideAction(
            caption: 'Edit',
            color: grey,
            icon: Icons.edit,
            onTap: () {
              EditBookDialog.show(context, book);
            },
          ),
        ),
        IconSlideAction(
          caption: 'Open in Browser',
          color: yellow,
          icon: Icons.web,
          onTap: () => _onTapWeb(context, book),
        ),
      ],
      child: GestureDetector(
        onTap: () {
          if (kIsWeb) {
            showDialog(
              context: context,
              builder: (context) {
                return SimpleDialog(
                  contentPadding: const EdgeInsets.all(20),
                  title: Text('${book.title} - ${book.author}'),
                  children: [
                    ElevatedButton.icon(
                      style: ElevatedButton.styleFrom(
                        primary: book.read ? red : green,
                        onPrimary: book.read ? Colors.white : Colors.black,
                      ),
                      onPressed: () {
                        Navigator.of(context).pop();
                        _onTapStatus(context, watch, book);
                      },
                      icon: Icon(book.read ? Icons.close : Icons.done),
                      label: Text(book.read ? 'Unread' : 'Finished'),
                    ),
                    const SizedBox(height: 12),
                    Hero(
                      tag: 'hero-edit-tags-${book.id}',
                      child: ElevatedButton.icon(
                        style: ElevatedButton.styleFrom(
                          primary: blue,
                          onPrimary: Colors.black,
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                          EditTagsDialog.show(context, book);
                        },
                        icon: const Icon(Icons.tag),
                        label: const Text('Edit Tags'),
                      ),
                    ),
                    const SizedBox(height: 12),
                    Hero(
                      tag: 'hero-edit-book-${book.id}',
                      child: ElevatedButton.icon(
                        style: ElevatedButton.styleFrom(
                          primary: grey,
                          onPrimary: Colors.white,
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                          EditBookDialog.show(context, book);
                        },
                        icon: const Icon(Icons.edit),
                        label: const Text('Edit'),
                      ),
                    ),
                    const SizedBox(height: 12),
                    ElevatedButton.icon(
                      style: ElevatedButton.styleFrom(
                        primary: yellow,
                        onPrimary: Colors.black,
                      ),
                      onPressed: () {
                        Navigator.of(context).pop();
                        _onTapWeb(context, book);
                      },
                      icon: const Icon(Icons.web),
                      label: const Text('Open in Browser'),
                    ),
                  ],
                );
              },
            );
          }
        },
        child: BookWidget(book: book),
      ),
    );
  }
}
