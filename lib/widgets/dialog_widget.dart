import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class DialogWidget extends StatelessWidget {
  final String title;
  final Widget? child;
  final List<Widget>? additionalOptions;
  final Function onPressed;

  const DialogWidget({
    Key? key,
    required this.title,
    required this.onPressed,
    this.child,
    this.additionalOptions,
  }) : super(key: key);

  static void show(
      {required BuildContext context,
      required String title,
      required Function onPressed,
      Widget? child}) {
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (context) =>
          DialogWidget(title: title, onPressed: onPressed, child: child),
    );
  }

  static void showNoYesDefault({
    required BuildContext context,
    required String title,
    required Function onPressed,
    required Function onPressedDefault,
    Widget? child,
  }) {
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (context) => DialogWidget(
        title: title,
        onPressed: onPressed,
        additionalOptions: [
          OutlinedButton(
            onPressed: () {
              Navigator.of(context).pop();
              onPressedDefault();
            },
            child: const Text('DEFAULT'),
          ),
        ],
        child: child,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: Theme.of(context).cardColor,
      title: Text(title),
      content: SingleChildScrollView(
        child: child ?? Container(),
      ),
      actions: [
        ElevatedButton(
          onPressed: () => Navigator.of(context).pop(),
          child: const Text('NO'),
        ),
        const SizedBox(
          width: 4,
        ),
        OutlinedButton(
          onPressed: () {
            Navigator.of(context).pop();
            onPressed();
          },
          child: const Text('YES'),
        ),
        const SizedBox(
          width: 4,
        ),
        if (additionalOptions != null) ...additionalOptions!,
      ],
    );
  }
}
