const statusRead = 'Finished';
const statusUnRead = 'Unread';

const sortCreationTime = 'Creation Time';
const sortHistory = 'History';
const sortTitle = 'Title';
const sortAuthor = 'Author';
const sortLength = 'Length';
const sortRating = 'Rating';
