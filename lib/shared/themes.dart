import 'package:flutter/material.dart';
import 'package:theme_provider/theme_provider.dart';

const red = Color(0xFFF07178);
const green = Color(0xFF8DE890);
const blue = Color(0xFF82AAFF);
const yellow = Color(0xFFFFCB6B);
const darkGray = Color(0xFF151618);
const grey = Color(0xFF323335);

final darkThemeWhiteAccent = _appThemeBuilder(
  id: 'dark',
  description: 'Dark / Red / White (default)',
  brightness: Brightness.dark,
  baseTheme: ThemeData.dark(),
  baseTextTheme: Typography.whiteHelsinki,
  baseColorScheme: const ColorScheme.dark(),
  primary: red,
  onPrimary: Colors.white,
  background: darkGray,
  onBackground: Colors.white,
  cardColor: grey,
);

final darkTheme = _appThemeBuilder(
  id: 'dark_alternative',
  description: 'Dark / Red / Dark',
  brightness: Brightness.dark,
  baseTheme: ThemeData.dark(),
  baseTextTheme: Typography.whiteHelsinki,
  baseColorScheme: const ColorScheme.dark(),
  primary: red,
  onPrimary: darkGray,
  background: darkGray,
  onBackground: Colors.white,
  cardColor: grey,
);

final darkGreenTheme = _appThemeBuilder(
  id: 'dark_green',
  description: 'Dark / Green / Dark',
  brightness: Brightness.dark,
  baseTheme: ThemeData.dark(),
  baseTextTheme: Typography.whiteHelsinki,
  baseColorScheme: const ColorScheme.dark(),
  primary: green,
  onPrimary: darkGray,
  background: darkGray,
  onBackground: Colors.white,
  cardColor: grey,
);

final darkBlueTheme = _appThemeBuilder(
  id: 'dark_blue',
  description: 'Dark / Blue / Dark',
  brightness: Brightness.dark,
  baseTheme: ThemeData.dark(),
  baseTextTheme: Typography.whiteHelsinki,
  baseColorScheme: const ColorScheme.dark(),
  primary: blue,
  onPrimary: darkGray,
  background: darkGray,
  onBackground: Colors.white,
  cardColor: grey,
);

final lightTheme = _appThemeBuilder(
  id: 'light',
  description: 'White / Red / White',
  brightness: Brightness.light,
  baseTheme: ThemeData.light(),
  baseTextTheme: Typography.blackHelsinki,
  baseColorScheme: const ColorScheme.light(),
  primary: red,
  onPrimary: Colors.white,
  background: Colors.white,
  onBackground: Colors.black,
  cardColor: Colors.white,
);

AppTheme _appThemeBuilder(
    {required String id,
    required String description,
    required Brightness brightness,
    required ThemeData baseTheme,
    required ColorScheme baseColorScheme,
    required TextTheme baseTextTheme,
    required Color primary,
    required Color onPrimary,
    required Color background,
    required Color onBackground,
    required Color cardColor}) {
  return AppTheme(
    id: id,
    description: description,
    data: ThemeData.from(
      colorScheme: baseColorScheme.copyWith(
        primary: primary,
        onPrimary: onPrimary,
        background: background,
        onBackground: onBackground,
        secondary: primary,
      ),
    ).copyWith(
      primaryColor: primary,
      splashColor: onBackground.withAlpha(100),
      backgroundColor: background,
      canvasColor: background,
      cardColor: cardColor,
      cardTheme: baseTheme.cardTheme.copyWith(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),
      ),
      sliderTheme: SliderThemeData.fromPrimaryColors(
        primaryColor: primary,
        primaryColorDark: cardColor,
        primaryColorLight: onPrimary,
        valueIndicatorTextStyle:
            baseTheme.textTheme.caption!.copyWith(color: onPrimary),
      ),
      textTheme: baseTextTheme
          .copyWith(
            headline5:
                const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            headline6:
                const TextStyle(fontSize: 16, fontWeight: FontWeight.normal),
          )
          .apply(bodyColor: baseTextTheme.bodyText1?.color),
      appBarTheme: baseTheme.appBarTheme.copyWith(
        brightness: brightness,
        backgroundColor: primary,
        foregroundColor: onPrimary,
        iconTheme: IconThemeData(color: onPrimary),
      ),
    ),
  );
}
