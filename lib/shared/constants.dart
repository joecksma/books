import 'package:flutter/material.dart';

const kHintTextStyle = TextStyle(
  color: Colors.white54,
  fontFamily: 'OpenSans',
);

const kLabelStyle = TextStyle(
  color: Colors.white,
  fontWeight: FontWeight.bold,
  fontFamily: 'OpenSans',
);

BoxDecoration kBoxDecorationStyle(BuildContext context) => BoxDecoration(
      color: Theme.of(context).chipTheme.backgroundColor,
      borderRadius: BorderRadius.circular(10.0),
      boxShadow: const [BoxShadow(color: Colors.black12)],
    );

OutlineInputBorder kfocusedBorder(BuildContext context) => OutlineInputBorder(
      borderRadius: BorderRadius.circular(10.0),
      borderSide: BorderSide(color: Theme.of(context).primaryColor),
    );

InputDecoration kInputDecoration(BuildContext context) => InputDecoration(
      contentPadding: const EdgeInsets.fromLTRB(12, 20, 12, 20),
      border: InputBorder.none,
      focusedBorder: kfocusedBorder(context),
    );
