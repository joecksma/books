import 'package:books/controllers/auth_controller.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final firebaseAuthProvider = Provider<FirebaseAuth>((ref) {
  return FirebaseAuth.instance;
});

final authControllerProvider = Provider<AuthController>((ref) {
  return AuthController(ref.read);
});

final authStateProvider = StreamProvider<User?>((ref) {
  return ref.watch(authControllerProvider).authStateChange;
});
