import 'package:books/controllers/book_controller.dart';
import 'package:books/controllers/filter_controller.dart';
import 'package:books/models/book/book_model.dart';
import 'package:books/models/filter/filter_list_model.dart';
import 'package:books/providers/filter_provider.dart';
import 'package:books/shared/enums.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

final bookListProvider = StateProvider<List<Book>>((ref) {
  final bookListState = ref.watch(bookListControllerProvider);
  return bookListState.maybeWhen(
    data: (books) {
      return books;
    },
    orElse: () => <Book>[],
  );
});

final filteredBookListProvider = StateProvider<List<Book>>((ref) {
  final filter = ref.watch(filterControllerProvider);

  final bookSearchState = ref.watch(bookSearchProvider).state;
  List<Book> result = ref.watch(bookListProvider).state;

  if (filter.status.isNotEmpty) {
    result = _filterReadStatus(result, filter.status);
  }
  if (filter.tags.active) {
    result = _filterTags(result, filter.tags);
  }
  if (filter.ratings.active) {
    result = _filterRating(result, filter.ratings);
  }
  if (filter.characters.active) {
    result = _filterCharacter(result, filter.characters);
  }
  if (filter.genres.active) {
    result = _filterGenre(result, filter.genres);
  }
  result = _filterRange(result, filter.minLength, filter.maxLength);

  if (bookSearchState.isNotEmpty) {
    result = _filterSearch(result, bookSearchState);
  }
  return _sort(result, filter.sort.include.first, filter.sort.and);
});

List<Book> _filterReadStatus(List<Book> books, Set<String> status) {
  final bool read = status.contains(statusRead);
  final bool unread = status.contains(statusUnRead);
  if (read && unread) {
    return books;
  }
  return books.where((book) {
    return (read && book.read) || (unread && !book.read);
  }).toList();
}

List<Book> _filterSearch(List<Book> books, String search) {
  // split into tokens
  List<String> searchTokens = [];
  String searchLower = search.toLowerCase();
  final RegExp quotedRegex = RegExp('"(.*?)"');
  final Iterable<RegExpMatch> matches = quotedRegex.allMatches(searchLower);
  searchTokens = matches.map((match) => match.group(1)!).toList();
  final RegExp replacer = RegExp('".*?"');
  searchLower = searchLower.replaceAll(replacer, '');
  if (search.contains(' ')) {
    searchTokens.addAll(searchLower.replaceAll('"', '').split(' '));
  } else {
    searchTokens.add(searchLower.replaceAll('"', ''));
  }
  // filter
  return books.where((book) {
    for (final String token in searchTokens) {
      final bool title = book.title.toLowerCase().contains(token);
      final bool author = book.author.toLowerCase().contains(token);
      final bool description = book.description.toLowerCase().contains(token);
      final bool characters = book.characters
          .any((character) => character.toLowerCase().contains(token));
      if (!(title || author || description || characters)) return false;
    }
    return true;
  }).toList();
}

List<Book> _filterRange(List<Book> books, double min, double max) {
  return books
      .where((book) => book.length >= min && book.length <= max)
      .toList();
}

List<Book> _filterRating(List<Book> books, FilterList filter) {
  return books.where((book) => _satisfies([book.rating], filter)).toList();
}

List<Book> _filterCharacter(List<Book> books, FilterList filter) {
  return books.where((book) => _satisfies(book.characters, filter)).toList();
}

List<Book> _filterGenre(List<Book> books, FilterList filter) {
  return books.where((book) => _satisfies(book.genre, filter)).toList();
}

List<Book> _filterTags(List<Book> books, FilterList filter) {
  return books.where((book) => _satisfies(book.lists, filter)).toList();
}

bool _satisfies(List<String> list, FilterList filter) {
  if (list.any((element) => filter.exclude.contains(element))) return false;
  if (filter.include.isEmpty) return true;
  if (filter.and) {
    return filter.include.every((element) => list.contains(element));
  } else {
    return filter.include.any((element) => list.contains(element));
  }
}

List<Book> _sort(List<Book> books, String sort, bool decending) {
  final before = decending ? 1 : -1;
  final after = decending ? -1 : 1;

  int _compareTime(DateTime a, DateTime b) {
    if (a.isAtSameMomentAs(b)) {
      return 0;
    }
    if (a.isBefore(b)) {
      return before;
    } else {
      return after;
    }
  }

  int _compareString(String a, String b) {
    final comparison = a.toLowerCase().compareTo(b.toLowerCase());
    if (comparison < 0) {
      return before;
    }
    if (comparison > 0) {
      return after;
    }
    return comparison;
  }

  int _compareInt(int a, int b) {
    if (a < b) {
      return before;
    }
    if (a > b) {
      return after;
    }
    return 0;
  }

  switch (sort) {
    case sortCreationTime:
      return books..sort((Book a, Book b) => _compareTime(a.time!, b.time!));
    case sortHistory:
      return books
        ..sort((Book a, Book b) => _compareTime(a.updatedAt!, b.updatedAt!));
    case sortTitle:
      return books..sort((Book a, Book b) => _compareString(a.title, b.title));
    case sortAuthor:
      return books
        ..sort((Book a, Book b) => _compareString(a.author, b.author));
    case sortLength:
      return books..sort((Book a, Book b) => _compareInt(a.length, b.length));
    case sortRating:
      return books
        ..sort((Book a, Book b) => _compareString(a.rating, b.rating));
    default:
      return books..sort((Book a, Book b) => _compareTime(a.time!, b.time!));
  }
}
