import 'dart:math';
import 'package:books/controllers/filter_controller.dart';
import 'package:books/models/filter/filter_list_model.dart';
import 'package:books/models/filter/filter_model.dart';
import 'package:books/providers/filtered_book_list_provider.dart';
import 'package:books/shared/enums.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

final bookSearchProvider = StateProvider<String>((_) => '');

final sortActiveProvider = StateProvider<bool>((ref) {
  final sort = ref.watch(filterControllerProvider).sort;
  return sort.include.first != sortCreationTime || !sort.and;
});

final anyFilterActiveProvider = StateProvider<bool>((ref) {
  final filter = ref.watch(filterControllerProvider);
  final available = ref.watch(availableFilterProvider).state;
  return filter.tags.exclude.isNotEmpty ||
      filter.ratings.exclude.isNotEmpty ||
      filter.characters.exclude.isNotEmpty ||
      filter.genres.exclude.isNotEmpty ||
      filter.tags.include.isNotEmpty ||
      filter.ratings.include.isNotEmpty ||
      filter.characters.include.isNotEmpty ||
      filter.genres.include.isNotEmpty ||
      (filter.minLength != double.negativeInfinity &&
          available.minLength != filter.minLength) ||
      (filter.maxLength != double.infinity &&
          available.maxLength != filter.maxLength);
});

final availableFilterProvider = StateProvider<Filter>((ref) {
  // include represents the available filter. exclude is not used
  final bookListState = ref.watch(bookListProvider).state;
  final lengths = bookListState.map((book) => book.length.toDouble()).toList();

  // calculate lengths
  double minLength = double.infinity;
  double maxLength = double.negativeInfinity;
  for (final length in lengths) {
    minLength = min(length, minLength);
    maxLength = max(length, maxLength);
  }
  minLength = minLength == double.infinity ? 0.0 : minLength;
  maxLength = maxLength == double.negativeInfinity ? 1.0 : maxLength;

  return Filter(
    status: {statusUnRead, statusRead},
    minLength: minLength,
    maxLength: maxLength,
    ratings: FilterList(
      include: bookListState.map((book) => book.rating).toSet(),
    ),
    characters: FilterList(
      include: bookListState
          .map((book) => book.characters)
          .expand((character) => character)
          .toSet(),
    ),
    genres: FilterList(
      include: bookListState
          .map((book) => book.genre)
          .expand((genre) => genre)
          .toSet(),
    ),
    tags: FilterList(
      include:
          bookListState.map((book) => book.lists).expand((tag) => tag).toSet(),
    ),
    sort: const FilterList(
      include: {
        sortCreationTime,
        sortHistory,
        sortTitle,
        sortAuthor,
        sortLength,
        sortRating
      },
    ),
  );
});
