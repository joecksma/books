import 'package:books/models/book/book_model.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final currentBookProvider = ScopedProvider<Book>(null);
