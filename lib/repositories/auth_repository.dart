import 'package:books/extensions/firebase_firestore_extension.dart';
import 'package:books/models/failures/auth_failure.dart';
import 'package:books/providers/auth_provider.dart';
import 'package:books/providers/firestore_provider.dart';
import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

abstract class BaseAuthRepository {
  Stream<User?> get authStateChanges;
  Future<Either<AuthFailure, String>> signIn(
      {required String email, required String password});
  Future<Either<AuthFailure, String>> signUp(
      {required String email, required String password});
  Future<Either<AuthFailure, String>> signOut();
}

final authRepositoryProvider =
    Provider<AuthRepository>((ref) => AuthRepository(ref.read));

class AuthRepository implements BaseAuthRepository {
  final Reader _read;
  AuthRepository(this._read);

  @override
  Stream<User?> get authStateChanges =>
      _read(firebaseAuthProvider).authStateChanges();

  @override
  Future<Either<AuthFailure, String>> signIn(
      {required String email, required String password}) async {
    try {
      await _read(firebaseAuthProvider).signInWithEmailAndPassword(
        email: email,
        password: password,
      );
      return right("Login Successful");
    } on FirebaseAuthException catch (e) {
      return left(AuthFailure(message: e.message!));
    }
  }

  @override
  Future<Either<AuthFailure, String>> signUp(
      {required String email, required String password}) async {
    try {
      final creds =
          await _read(firebaseAuthProvider).createUserWithEmailAndPassword(
        email: email,
        password: password,
      );
      await _read(firebaseFireStoreProvider)
          .usersRef()
          .doc(creds.user!.uid)
          .set({"updatedAt": DateTime.now()});
      return right("Signup successful");
    } on FirebaseAuthException catch (e) {
      return left(AuthFailure(message: e.message!));
    }
  }

  @override
  Future<Either<AuthFailure, String>> signOut() async {
    try {
      await _read(firebaseAuthProvider).signOut();
      return right("Signout successful");
    } on FirebaseAuthException catch (e) {
      return left(AuthFailure(message: e.message!));
    }
  }
}
