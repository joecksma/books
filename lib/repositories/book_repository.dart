import 'package:books/extensions/firebase_firestore_extension.dart';
import 'package:books/models/book/book_model.dart';
import 'package:books/models/failures/firebase_failure.dart';
import 'package:books/providers/firestore_provider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dartz/dartz.dart';
import 'package:firestore_cache/firestore_cache.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class BaseBookRepository {
  Future<Either<FirebaseFailure, String>> createBook(
      {required String userId, required Book book});
  Future<Either<FirebaseFailure, List<Book>>> readBooks(
      {required String userId});
  Future<Either<FirebaseFailure, void>> updateBook(
      {required String userId, required Book book});
  Future<Either<FirebaseFailure, void>> deleteBook(
      {required String userId, required String bookId});
}

final bookRepositoryProvider =
    Provider<BookRepository>((ref) => BookRepository(ref.read));

class BookRepository implements BaseBookRepository {
  static const _cacheKey = 'updatedAt';
  final Reader _read;

  const BookRepository(this._read);

  Future<QuerySnapshot> _getDocs(
    String userId,
    Query query,
  ) async {
    return FirestoreCache.getDocuments(
      query: query,
      cacheDocRef: _read(firebaseFireStoreProvider).userRef(userId),
      firestoreCacheField: _cacheKey,
      localCacheKey: _cacheKey,
    );
  }

  Future<void> _updateCache(String userId) async {
    final time = DateTime.now();
    await _read(firebaseFireStoreProvider)
        .userRef(userId)
        .update({_cacheKey: time});
    final prefs = await SharedPreferences.getInstance();
    // save last fetch as the store time + 1μs
    await prefs.setString(
        _cacheKey, time.add(const Duration(microseconds: 1)).toIso8601String());
  }

  @override
  Future<Either<FirebaseFailure, String>> createBook(
      {required String userId, required Book book}) async {
    try {
      final docRef = await _read(firebaseFireStoreProvider)
          .booksRef(userId)
          .add(book.toDocument());
      await _updateCache(userId);
      return right(docRef.id);
    } on FirebaseException catch (e) {
      return left(FirebaseFailure(message: e.message!));
    }
  }

  @override
  Future<Either<FirebaseFailure, List<Book>>> readBooks(
      {required String userId}) async {
    try {
      final snapshot = await _getDocs(
        userId,
        _read(firebaseFireStoreProvider)
            .booksRef(userId)
            .orderBy('time', descending: true),
      );
      return right(snapshot.docs.map((doc) => Book.fromDocument(doc)).toList());
    } on FirebaseException catch (e) {
      return left(FirebaseFailure(message: e.message!));
    }
  }

  @override
  Future<Either<FirebaseFailure, void>> updateBook(
      {required String userId, required Book book}) async {
    try {
      await _read(firebaseFireStoreProvider)
          .booksRef(userId)
          .doc(book.id)
          .update(book.toDocument());
      await _updateCache(userId);
      return right(null);
    } on FirebaseException catch (e) {
      return left(FirebaseFailure(message: e.message!));
    }
  }

  @override
  Future<Either<FirebaseFailure, void>> deleteBook(
      {required String userId, required String bookId}) async {
    try {
      await _read(firebaseFireStoreProvider)
          .booksRef(userId)
          .doc(bookId)
          .delete();
      await _updateCache(userId);
      return right(null);
    } on FirebaseException catch (e) {
      return left(FirebaseFailure(message: e.message!));
    }
  }
}
