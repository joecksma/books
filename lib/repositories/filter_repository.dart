import 'dart:convert';

import 'package:books/models/filter/filter_model.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class BaseFilterRepository {
  Future<bool> setFilter({required Filter filter});
  Future<Filter> readFilter();
  Future<void> deleteFilter();
}

final filterRepositoryProvider =
    Provider<FilterRepository>((_) => const FilterRepository());

class FilterRepository implements BaseFilterRepository {
  static const prefsKey = 'filter';

  const FilterRepository();

  @override
  Future<bool> setFilter({required Filter filter}) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.setString(prefsKey, json.encode(filter.toJson()));
  }

  @override
  Future<Filter> readFilter() async {
    final prefs = await SharedPreferences.getInstance();
    final String? filter = prefs.getString(prefsKey);
    return filter == null
        ? Filter.empty()
        : Filter.fromJson(json.decode(filter) as Map<String, dynamic>);
  }

  @override
  Future<void> deleteFilter() async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.remove(prefsKey);
  }
}
